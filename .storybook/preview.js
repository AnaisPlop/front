import React from 'react'
import { withRouter } from 'storybook-addon-react-router-v6'
import { addDecorator } from '@storybook/react'
import { ChakraProvider, theme } from '@chakra-ui/react'
import i18n from "./i18next.js";

export const decorators = [withRouter]

export const parameters = {
  i18n,
  reactRouter: {
    routePath: '/',
    routeParams: {},
    searchParams: {},
    routeState: {},
  },
  locale: "en",
  locales: {
    en: "English",
    fr: "Français",
  },
  actions: {argTypesRegex: "^on[A-Z].*"},
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

addDecorator(storyFn => (
    <ChakraProvider theme={theme} resetCSS>
      {storyFn()}
    </ChakraProvider>
))