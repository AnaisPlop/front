import {
    Box,
    Heading,
    Container,
    Text,
    Stack,
    useColorModeValue,
    Center,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Helmet } from 'react-helmet'
import rehypeRaw from 'rehype-raw'
import { ReactMarkdown } from 'react-markdown/lib/react-markdown'
import { FaArrowCircleDown } from 'react-icons/fa'
import StatsWithCard from '../components/molecules/Stats/StatsWithCard/StatsWithCard'
import EmphasisText from '../components/atoms/Text/EmphasisText'
import Hippopotame from '../components/atoms/Illustrations/Hippopotame'
import Drill from '../components/atoms/Illustrations/Drill'
import Pig from '../components/atoms/Illustrations/Pig'
import Stamp from '../components/atoms/Illustrations/Stamp'

const Why = () => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>{t('why.title')}</title>
            </Helmet>
            <Container maxW="5xl">
                <Stack
                    textAlign="center"
                    align="center"
                    spacing={{ base: 8, md: 10 }}
                    py={{ base: 20, md: 28 }}
                >
                    <Heading
                        fontWeight={600}
                        fontSize={{ base: '3xl', sm: '4xl', md: '6xl' }}
                        lineHeight="110%"
                    >
                        <EmphasisText
                            color={useColorModeValue('blue.400', 'yellow.200')}
                        >
                            {t('why.hero1.title')}
                        </EmphasisText>
                    </Heading>
                    <Text maxW="3xl">
                        <EmphasisText
                            color={useColorModeValue('blue.400', 'yellow.200')}
                        >
                            {t('why.hero1.description')}
                        </EmphasisText>
                    </Text>
                </Stack>
            </Container>
            <StatsWithCard
                heading={t('why.stats.title')}
                stats={[
                    {
                        title: t('why.stats.annualBudget.title'),
                        description: t('why.stats.annualBudget.description'),
                        icon: <Pig height="10px" />,
                        value: t('why.stats.annualBudget.value'),
                        source: {
                            name: t('why.stats.annualBudget.source.name'),
                            link: t('why.stats.annualBudget.source.link'),
                        },
                    },
                    {
                        title: t('why.stats.trailerFootprint.title'),
                        description: t(
                            'why.stats.trailerFootprint.description'
                        ),
                        icon: <Hippopotame />,
                        value: t('why.stats.trailerFootprint.value'),
                        source: {
                            name: t('why.stats.trailerFootprint.source.name'),
                            link: t('why.stats.trailerFootprint.source.link'),
                        },
                    },
                    {
                        title: t('why.stats.drillUse.title'),
                        description: t('why.stats.drillUse.description'),
                        icon: <Drill />,
                        value: t('why.stats.drillUse.value'),
                        source: {
                            name: t('why.stats.drillUse.source.name'),
                            link: t('why.stats.drillUse.source.link'),
                        },
                    },
                    {
                        title: t('why.stats.electronicItemsCount.title'),
                        description: t(
                            'why.stats.electronicItemsCount.description'
                        ),
                        icon: <Stamp height="14px" />,
                        value: t('why.stats.electronicItemsCount.value'),
                        source: {
                            name: t(
                                'why.stats.electronicItemsCount.source.name'
                            ),
                            link: t(
                                'why.stats.electronicItemsCount.source.link'
                            ),
                        },
                    },
                ]}
            />
            <Box
                maxW="2xl"
                mx="auto"
                px={{ base: '6', lg: '8' }}
                py={{ base: '16', sm: '20' }}
                textAlign="center"
            >
                <Heading
                    as="h2"
                    size="3xl"
                    fontWeight="extrabold"
                    letterSpacing="tight"
                >
                    {t('why.learnMore.title')}
                </Heading>
                <Text mt="4" fontSize="lg">
                    <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                        {t('why.learnMore.description')}
                    </ReactMarkdown>
                </Text>
                <Center mt={50}>
                    <FaArrowCircleDown
                        size="80px"
                        title={t('why.learnMore.scroll')}
                    />
                </Center>
            </Box>
            <iframe
                title="Ces objets qui pèsent lourd dans notre quotidien - Ademe"
                src="https://multimedia.ademe.fr/infographies/infographie-poids-carbone/"
                width="100%"
                height="5000px"
            />
        </>
    )
}

export default Why
