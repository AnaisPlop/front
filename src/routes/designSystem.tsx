import * as React from 'react'
import TwoColumnsWithImage from '../components/_templates/marketing/heroes/TwoColumnsWithImage'
import HeroWithStatBelow from '../components/_templates/marketing/heroes/HeroWithStatBelow'
import WithFeaturedLogos from '../components/_templates/marketing/heroes/WithFeaturedLogos'
import WithScreenshotBelow from '../components/_templates/marketing/heroes/WithScreenshotBelow'
import WithSlightCutImage from '../components/_templates/marketing/heroes/WithSlightCutImage'
import CtaWithInput from '../components/_templates/marketing/cta/CtaWithInput/CtaWithInput'
import CtaCallCenter from '../components/_templates/marketing/cta/CtaCallCenter/CtaCallCenter'
import CtaSimpleCentered from '../components/_templates/marketing/cta/CtaSimpleCentered/CtaSimpleCentered'
import CtaForMobileApp from '../components/_templates/marketing/cta/CtaForMobileApp/CtaForMobileApp'
import CtaWithTwoActions from '../components/_templates/marketing/cta/CtaWithTwoActions/CtaWithTwoActions'
import CtaWithRightButtons from '../components/_templates/marketing/cta/CtaWithRightButtons/CtaWithRightButtons'
import FeatureWithImage from '../components/_templates/marketing/features/FeatureWithImage/FeatureWithImage'
import TabsWithIcon from '../components/_templates/marketing/features/TabsWithIcon/TabsWithIcon'
import TestimonialWithVideo from '../components/_templates/marketing/features/TestimonialWithVideo/TestimonialWithVideo'
import ThreeColumnGridBelow from '../components/_templates/marketing/features/ThreeColumnGridBelow/ThreeColumnGridBelow'
import TwoByTwoFeatures from '../components/_templates/marketing/features/TwoByTwoFeatures/TwoByTwoFeatures'
import TwoByTwoGrid from '../components/_templates/marketing/features/TwoByTwoGrid/TwoByTwoGrid'
import WithSixLinks from '../components/_templates/marketing/features/WithSixLinks/WithSixLinks'
import WithBoxedLogo from '../components/_templates/marketing/logoGrid/WithBoxedLogo/WithBoxedLogo'
import WithBrandBackground from '../components/_templates/marketing/logoGrid/WithBrandBackground/WithBrandBackground'
import SimpleWithBadge from '../components/_templates/marketing/menu/SimpleWithBadge/SimpleWithBadge'
import WithStartIcon from '../components/_templates/marketing/menu/WithStartIcon/WithStartIcon'
import BlogWithTile from '../components/_templates/marketing/blog/BlogWithTile/BlogWithTile'
import BlogWithLargeImage from '../components/_templates/marketing/blog/BlogWithLargeImage/BlogWithLargeImage'
import BlogWithCard from '../components/_templates/marketing/blog/BlogWithCard/BlogWithCard'
import NewsletterWithDarkBg from '../components/_templates/marketing/newsletter/NewsletterWithDarkBg/NewsletterWithDarkBg'
import NewsletterWithImage from '../components/_templates/marketing/newsletter/NewsletterWithImage/NewsletterWithImage'
import MultiColored from '../components/_templates/marketing/pricing/MultiColored/MultiColored'
import PricingWithCornerBadge from '../components/_templates/marketing/pricing/PricingWithCornerBadge/PricingWithCornerBadge'
import ThreeTiersWithToggle from '../components/_templates/marketing/pricing/ThreeTiersWithToggle/ThreeTiersWithToggle'
import TwoTiersWithBottomBlock from '../components/_templates/marketing/pricing/TwoTiersWithBottomBlock/TwoTiersWithBottomBlock'
import StatsWithRightIcon from '../components/_templates/marketing/stats/StatsWithRightIcon/StatsWithRightIcon'
import StatsWithDivider from '../components/_templates/marketing/stats/StatsWithDivider/StatsWithDivider'
import StatsWithLines from '../components/_templates/marketing/stats/StatsWithLines/StatsWithLines'
import StatsWithCard from '../components/_templates/marketing/stats/StatsWithCard/StatsWithCard'
import FooterWithTwoColumns from '../components/_templates/marketing/footers/FooterWithTwoColumns/FooterWithTwoColumns'
import FooterWithFourColumns from '../components/_templates/marketing/footers/FooterWithFourColumns/FooterWithFourColumns'
import FooterWithLogoBelow from '../components/_templates/marketing/footers/FooterWithLogoBelow/FooterWithLogoBelow'
import SimpleFooter from '../components/_templates/marketing/footers/SimpleFooter/SimpleFooter'

const DesignSystem = () => (
    <>
        <TwoColumnsWithImage />
        <HeroWithStatBelow />
        <WithFeaturedLogos />
        <WithScreenshotBelow />
        <WithSlightCutImage />
        <CtaWithInput />
        <CtaCallCenter />
        <CtaSimpleCentered />
        <CtaForMobileApp />
        <CtaWithTwoActions />
        <CtaWithRightButtons />
        <FeatureWithImage />
        <TabsWithIcon />
        <TestimonialWithVideo />
        <ThreeColumnGridBelow />
        <TwoByTwoFeatures />
        <TwoByTwoGrid />
        <WithSixLinks />
        <WithBoxedLogo />
        <WithBrandBackground />
        <SimpleWithBadge />
        <WithStartIcon />
        <BlogWithTile />
        <BlogWithLargeImage />
        <BlogWithCard />
        <NewsletterWithDarkBg />
        <NewsletterWithImage />
        <MultiColored />
        <PricingWithCornerBadge />
        <ThreeTiersWithToggle />
        <TwoTiersWithBottomBlock />
        <StatsWithRightIcon />
        <StatsWithDivider />
        <StatsWithLines />
        <StatsWithCard />
        <FooterWithTwoColumns />
        <FooterWithFourColumns />
        <FooterWithLogoBelow />
        <SimpleFooter />
    </>
)

export default DesignSystem
