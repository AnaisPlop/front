import { Box, Heading, Container, Text, Button, Stack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Helmet } from 'react-helmet'
import { FaGitlab } from 'react-icons/fa'
import Heart from '../components/atoms/Illustrations/Heart'
import EmphasisText from '../components/atoms/Text/EmphasisText'

const Contribute = () => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>{t('contribute.title')}</title>
            </Helmet>
            <Container maxW="3xl">
                <Stack
                    as={Box}
                    textAlign="center"
                    spacing={{ base: 8, md: 14 }}
                    py={{ base: 20, md: 36 }}
                >
                    <Heading
                        fontWeight={600}
                        fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
                        lineHeight="110%"
                    >
                        <EmphasisText color="green.400">
                            {t('contribute.how.title')}
                        </EmphasisText>
                    </Heading>
                    <Text color="gray.500">
                        <EmphasisText color="yellow.400">
                            {t('contribute.how.description')}
                        </EmphasisText>
                    </Text>

                    <Heart
                        height={{ sm: '24rem', lg: '28rem' }}
                        mt={{ base: 12, sm: 16 }}
                    />
                    <Stack
                        direction="row"
                        spacing={3}
                        align="center"
                        alignSelf="center"
                        position="relative"
                    >
                        <Button
                            colorScheme="green"
                            bg="green.400"
                            rounded="full"
                            px={6}
                            _hover={{
                                bg: 'green.500',
                            }}
                        >
                            <a href="mailto:leny@bernard.host">
                                {t('contribute.how.joinUs')}
                            </a>
                        </Button>
                        <Button
                            rounded="full"
                            px={6}
                            colorScheme="yellow"
                            _hover={{ bg: 'pink.500', color: 'white' }}
                            leftIcon={<FaGitlab />}
                        >
                            <a href="https://communo.getigne-collectif.fr">
                                {t('contribute.how.fork')}
                            </a>
                        </Button>
                    </Stack>
                </Stack>
            </Container>
        </>
    )
}

export default Contribute
