import { Box, Heading, Container, Text, Stack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Helmet } from 'react-helmet'
import { BiHappyBeaming, BiMap, BiMoney } from 'react-icons/bi'
import { FaHandsHelping } from 'react-icons/fa'
import Heart from '../components/atoms/Illustrations/Heart'
import useAuth from '../auth/useAuth'
import WithImageBackground from '../components/molecules/Hero/WithImageBackground'
import DarkWithTestimonial from '../components/molecules/Feature/DarkWithTestimonial'
import ArrowButton from '../components/atoms/Button/ArrowButton'
import Button, { RainbowButon } from '../components/atoms/Button'

const Home = () => {
    const { t } = useTranslation()
    const auth = useAuth()
    return (
        <>
            <Helmet>
                <title>{t('home.title')}</title>
            </Helmet>
            <WithImageBackground
                backgroundImage={{
                    src: 'https://images.unsplash.com/photo-1590650153855-d9e808231d41?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=2250&q=80',
                    // src: '/illustrations/garage.jpg',
                    alt: 'Main Image',
                }}
                heading={
                    <>
                        {t('home.hero1.line1')}{' '}
                        <Text as="span" color="yellow.200">
                            {t('home.hero1.line2')}
                        </Text>
                    </>
                }
                content={t('home.hero1.description')}
                buttons={[
                    <RainbowButon
                        key="btn-hero-getStarted"
                        rounded="full"
                        px={6}
                        colorScheme="yellow"
                        _hover={{
                            bgGradient: 'linear(to-l, purple.400, pink.400)',
                            color: 'yellow',
                            transform: 'translateY(2px)',
                            boxShadow: 'lg',
                        }}
                        link={auth.user ? '/materials' : '/login'}
                    >
                        {t('home.hero1.getStarted')}
                    </RainbowButon>,
                    <Button
                        key="btn-hero-about"
                        colorScheme="white"
                        variant="ghost"
                        rounded="full"
                        px={6}
                        link="/why"
                        _hover={{
                            textDecoration: 'underline',
                        }}
                    >
                        {t('home.hero1.learnMore')}
                    </Button>,
                ]}
            />
            <DarkWithTestimonial
                title={t('home.features.title')}
                features={[
                    {
                        icon: <BiMap />,
                        title: t('home.features.local.title'),
                        content: t('home.features.local.content'),
                    },
                    {
                        icon: <BiMoney />,
                        title: t('home.features.saveMoney.title'),
                        content: t('home.features.saveMoney.content'),
                    },
                    {
                        icon: <FaHandsHelping />,
                        title: t('home.features.advices.title'),
                        content: t('home.features.advices.content'),
                    },
                    {
                        icon: <BiHappyBeaming />,
                        title: t('home.features.meet.title'),
                        content: t('home.features.meet.content'),
                    },
                ]}
                testimonials={[
                    {
                        author: {
                            avatar: `${process.env.REACT_APP_API_URL}/images/users/fixture_cyril.jpg`,
                            name: 'Laurent Brun',
                            location: 'Gétigné, 44',
                        },
                        content: 'Lorem ipsum',
                    },
                    {
                        author: {
                            avatar: 'https://images.unsplash.com/photo-1589729482945-ca6f3a235f7a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2550&q=80',
                            name: 'Laeticia Jaunet',
                            location: 'Cugand, 85',
                        },
                        content: 'Lorem ipsum dolor sit amet,',
                    },
                    {
                        author: {
                            avatar: `${process.env.REACT_APP_API_URL}/images/users/fixture_leny.jpg`,
                            name: 'Thibault Marchand',
                            location: 'Montauban, 44',
                        },
                        content:
                            'Très heureux de pouvoir enfin ouvrir mon atelier à la communauté et de pouvoir emprunter du bon matos, ça fait du bien de jouer collectif et de se faire confiance !',
                    },
                    {
                        author: {
                            avatar: `${process.env.REACT_APP_API_URL}/images/users/fixture_marion.jpg`,
                            name: 'Margaux Dranreb',
                            location: 'Gétigné, 44',
                        },
                        content:
                            "Le réflexe est arrivé très vite et j'espère que de plus en plus de monde va s'y mettre par chez moi, c'est tellement logique de mutualiser les équipements plutôt qu'on achète tous, chacun comme des cons, les mêmes produits moyenne gamme...!",
                    },
                ]}
            />
            <Container maxW="3xl">
                <Stack
                    as={Box}
                    textAlign="center"
                    spacing={{ base: 8, md: 14 }}
                    py={{ base: 20, md: 36 }}
                >
                    <Heading
                        fontWeight={600}
                        fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
                        lineHeight="110%"
                    >
                        {t('home.hero.line1')}
                        <br />
                        <Text as="span" color="green.400">
                            {t('home.hero.line2')}
                        </Text>
                    </Heading>
                    <Text color="gray.500">{t('home.hero.description')}</Text>

                    <Heart
                        height={{ sm: '24rem', lg: '28rem' }}
                        mt={{ base: 12, sm: 16 }}
                    />
                    <Stack
                        direction="column"
                        spacing={3}
                        align="center"
                        alignSelf="center"
                        position="relative"
                    >
                        <ArrowButton
                            button={
                                <Button
                                    colorScheme="green"
                                    bg="green.400"
                                    rounded="full"
                                    px={6}
                                    link={
                                        auth.user
                                            ? '/materials/new'
                                            : '/register'
                                    }
                                    _hover={{
                                        bg: 'green.500',
                                    }}
                                >
                                    {t('home.hero.getStarted')}
                                </Button>
                            }
                            incentiveLabel={t('home.hero.buttonIncentive')}
                        />
                        <Button
                            variant="link"
                            colorScheme="blue"
                            size="sm"
                            link="/how-it-works"
                        >
                            {t('home.hero.how')}
                        </Button>
                    </Stack>
                </Stack>
            </Container>
        </>
    )
}

export default Home
