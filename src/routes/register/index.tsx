import React from 'react'
import {
    Avatar,
    AvatarGroup,
    Box,
    Button,
    Center,
    Container,
    Flex,
    Heading,
    Stack,
    Text,
    useBreakpointValue,
    useColorModeValue,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { Link } from 'react-router-dom'
import RegisterForm from '../../components/molecules/Form/Register/registerForm'
import { LoginForm } from '../../components/molecules/Form/Login/loginForm'

const avatars = [
    {
        name: 'Leny',
        url: `${process.env.REACT_APP_API_URL}/images/users/fixture_leny.jpg`,
    },
    {
        name: 'Marion',
        url: `${process.env.REACT_APP_API_URL}/images/users/fixture_marion.jpg`,
    },
    {
        name: 'Kent Dodds',
        url: 'https://bit.ly/kent-c-dodds',
    },
    {
        name: 'Prosper Otemuyiwa',
        url: 'https://bit.ly/prosper-baba',
    },
    {
        name: 'Christian Nwamba',
        url: 'https://bit.ly/code-beast',
    },
    {
        name: 'Cyril',
        url: `${process.env.REACT_APP_API_URL}/images/users/fixture_cyril.jpg`,
    },
]

const Signin = ({ mode }: { mode: 'login' | 'register' }) => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>
                    {t(`signin.${mode}`)} {'< Communo'}
                </title>
            </Helmet>
            <Box position="relative">
                <Container maxW="7xl">
                    <Flex
                        flexDirection={{ base: 'column' }}
                        maxW="7xl"
                        py={{ base: 10, sm: 20, lg: 32 }}
                    >
                        <Stack spacing={{ base: 2 }}>
                            <Heading
                                lineHeight={1.1}
                                fontSize={{
                                    base: '3xl',
                                    sm: '4xl',
                                    md: mode === 'login' ? '5xl' : '3xl',
                                    lg: mode === 'login' ? '6xl' : '4xl',
                                }}
                                textAlign="center"
                            >
                                <>
                                    {t('signin.hero.title.part1')}
                                    <br />
                                    <Text
                                        as="span"
                                        bgGradient="linear(to-r, red.400,pink.400)"
                                        bgClip="text"
                                    >
                                        {t('signin.hero.title.part2')}
                                    </Text>{' '}
                                    {t('signin.hero.title.part3')}
                                </>
                            </Heading>
                            <Center>
                                <Stack
                                    direction="row"
                                    spacing={4}
                                    align="center"
                                >
                                    <AvatarGroup>
                                        {avatars.map((avatar) => (
                                            <Avatar
                                                key={avatar.name}
                                                name={avatar.name}
                                                src={avatar.url}
                                                size="md"
                                                position="relative"
                                                zIndex={2}
                                                _before={{
                                                    content: '""',
                                                    width: 'full',
                                                    height: 'full',
                                                    rounded: 'full',
                                                    transform: 'scale(1.125)',
                                                    bgGradient:
                                                        'linear(to-bl, red.400,pink.400)',
                                                    position: 'absolute',
                                                    zIndex: -1,
                                                    top: 0,
                                                    left: 0,
                                                }}
                                            />
                                        ))}
                                    </AvatarGroup>
                                    <Text
                                        fontFamily="heading"
                                        fontSize={{ base: '4xl', md: '6xl' }}
                                    >
                                        +
                                    </Text>
                                    <Flex
                                        align="center"
                                        justify="center"
                                        fontFamily="heading"
                                        fontSize={{ base: 'sm', md: 'lg' }}
                                        bg="gray.800"
                                        color="white"
                                        rounded="full"
                                        width={useBreakpointValue({
                                            base: '44px',
                                            md: '60px',
                                        })}
                                        height={useBreakpointValue({
                                            base: '44px',
                                            md: '60px',
                                        })}
                                        position="relative"
                                        _before={{
                                            content: '""',
                                            width: 'full',
                                            height: 'full',
                                            rounded: 'full',
                                            transform: 'scale(1.125)',
                                            bgGradient:
                                                'linear(to-bl, orange.400,yellow.400)',
                                            position: 'absolute',
                                            zIndex: -1,
                                            top: 0,
                                            left: 0,
                                        }}
                                    >
                                        <Text casing="uppercase">
                                            {t('register.you')}
                                        </Text>
                                    </Flex>
                                </Stack>
                            </Center>
                        </Stack>

                        <Container maxW={mode === 'login' ? 'md' : 'full'}>
                            <Stack
                                bg={useColorModeValue('white', 'gray.600')}
                                rounded="xl"
                                p={{ base: 4, sm: 6, md: 8 }}
                                mt={{ base: 4, sm: 6, md: 0 }}
                                spacing={{ base: 8 }}
                            >
                                <Stack spacing={4}>
                                    <Heading
                                        color={useColorModeValue(
                                            'gray.600',
                                            'white'
                                        )}
                                        lineHeight={1.1}
                                        fontSize={{
                                            base: 'xl',
                                            sm: '2xl',
                                            md: '3xl',
                                        }}
                                    >
                                        <>
                                            {t('signin.title')}
                                            <Text
                                                as="span"
                                                bgGradient="linear(to-r, yellow.200,yellow.400)"
                                                bgClip="text"
                                            >
                                                {' '}
                                                !
                                            </Text>
                                        </>
                                    </Heading>
                                    <Text
                                        color={useColorModeValue(
                                            'gray.600',
                                            'gray.200'
                                        )}
                                        fontSize={{ base: 'sm', sm: 'md' }}
                                    >
                                        <>
                                            {t('signin.subtitle')}{' '}
                                            <Text
                                                as="span"
                                                bgGradient="linear(to-r, red.400,pink.400)"
                                                bgClip="text"
                                            >
                                                {t('signin.emphaseWord')}
                                            </Text>
                                        </>
                                    </Text>
                                </Stack>
                                {mode === 'login' ? (
                                    <>
                                        <LoginForm />
                                        <Link to="/register">
                                            <Button variant="ghost" w="full">
                                                {t('login.form.register')}
                                            </Button>
                                        </Link>
                                    </>
                                ) : (
                                    <RegisterForm />
                                )}
                            </Stack>
                        </Container>
                    </Flex>
                </Container>
            </Box>
        </>
    )
}

export default Signin
