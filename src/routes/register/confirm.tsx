import React, { useState } from 'react'
import { Helmet } from 'react-helmet'
import { Flex, Heading, Stack, useColorModeValue } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import ConfirmForm from '../../components/molecules/Form/Register/confirmForm'
import Blur from '../../components/layout/Blur'
import WellDone from '../../components/atoms/Animation/WellDone'

const Confirm = () => {
    const { t } = useTranslation()
    const params = useParams()
    const [confirmed, confirm] = useState(false)
    const navigate = useNavigate()

    return (
        <>
            <Helmet>
                <title>
                    {t('register.confirm.title')} {'< Communo'}
                </title>
            </Helmet>
            <Flex
                minH="100vh"
                align="center"
                justify="center"
                bg={useColorModeValue('gray.50', 'gray.800')}
            >
                <Blur
                    position="absolute"
                    bottom={-10}
                    left={-10}
                    style={{ filter: 'blur(70px)' }}
                />
                <Stack spacing={8} mx="auto" maxW="lg" py={12} px={6}>
                    {(!confirmed && (
                        <Stack align="center">
                            <Heading fontSize="4xl">Enter confirm code</Heading>
                            {params.id && (
                                <ConfirmForm
                                    userId={params.id}
                                    confirm={confirm}
                                />
                            )}
                        </Stack>
                    )) || (
                        <WellDone
                            eventListeners={[
                                {
                                    eventName: 'complete',
                                    callback: () => {
                                        navigate('/login')
                                    },
                                },
                            ]}
                        />
                    )}
                </Stack>
            </Flex>
        </>
    )
}

export default Confirm
