import * as React from 'react'
import { Box, Button } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { useColorModeValue as mode } from '@chakra-ui/color-mode'
import rehypeRaw from 'rehype-raw'
import ReactMarkdown from 'react-markdown'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import WithSlightCutImage from '../../components/molecules/Hero/WithSlightCutImage'
import useAuth from '../../auth/useAuth'

const Pooling = () => {
    const { t } = useTranslation()
    const auth = useAuth()

    return (
        <>
            <Helmet>
                <title>
                    {t('pooling.index.title')} {t('meta.title.suffix')}
                </title>
            </Helmet>
            <WithSlightCutImage
                heading={
                    <>
                        <div>{t('pooling.index.heroDefinition.label')}</div>
                        <Box
                            as="mark"
                            color={mode('blue.500', 'blue.300')}
                            bg="transparent"
                        >
                            {t('pooling.index.heroDefinition.emphasis')}
                        </Box>
                    </>
                }
                image={{
                    src: 'https://images.unsplash.com/photo-1618969322232-92a7aa295ce1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
                    alt: 'alternative text',
                    credit: (
                        <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                            {`${t('global.credit.author', {
                                author: 'Rod Long',
                                link: 'https://unsplash.com/@rodlong?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
                            })} ${t('global.credit.origin', {
                                origin: 'Unsplash',
                                link: 'https://unsplash.com/s/photos/difference?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
                            })}`}
                        </ReactMarkdown>
                    ),
                }}
                content={t('pooling.index.heroDefinition.content')}
                buttons={[
                    <Button
                        key="seeMaterialBtn"
                        size="lg"
                        colorScheme="yellow"
                        rounded="full"
                        height="14"
                        px="8"
                        fontSize="md"
                        as={Link}
                        to="/pooling/search"
                    >
                        {t('pooling.index.heroDefinition.button.seeMaterial')}
                    </Button>,
                    <Button
                        key="addMaterialBtn"
                        size="lg"
                        height="14"
                        px="8"
                        fontSize="md"
                        variant="ghost"
                        as={Link}
                        to={(auth.user && '/pooling/new') || '/login'}
                        onClick={() => {
                            if (!auth.user) toast.info(t('global.please.login'))
                        }}
                    >
                        {t('pooling.index.heroDefinition.button.addMaterial')}
                    </Button>,
                ]}
            />
        </>
    )
}

export default Pooling
