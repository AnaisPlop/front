import React from 'react'
import {
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
    Container,
    SlideFade,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { useQuery } from '@apollo/client'
import { Link, useParams } from 'react-router-dom'
import { FiHome } from 'react-icons/fi'
import { findMaterialById } from '../../repositories/Material/MaterialRepository'
import { Material } from '../../types'
import MaterialForm from '../../components/molecules/Form/Material'
import useAuth from '../../auth/useAuth'
import Unauthorized403 from '../../components/molecules/Error/Unauthorized403'
import Loader from '../../components/atoms/Loader/Loader'

const MaterialEdit = () => {
    const { t } = useTranslation()
    const auth = useAuth()
    const { id, step } = useParams<{
        id: string
        step: 'information' | 'media' | 'pricing'
    }>()
    const { loading, data } = useQuery(findMaterialById, {
        variables: {
            id: `/materials/${id}`,
        },
    })
    let material: Material | null = null
    if (!loading) {
        material = data.material
        if (material?.owner._id !== auth.user?._id) {
            return <Unauthorized403 />
        }
    }

    return (
        <>
            <Helmet>
                <title>
                    {t('pooling.edit.title', { material })} {'< Communo'}
                </title>
            </Helmet>
            {(loading && <Loader />) ||
                (material && (
                    <Container maxW="7xl" minW="300px">
                        <Breadcrumb py={2} color="yellow.600">
                            <BreadcrumbItem>
                                <BreadcrumbLink as={Link} to="/">
                                    <FiHome />
                                </BreadcrumbLink>
                            </BreadcrumbItem>

                            <BreadcrumbItem>
                                <BreadcrumbLink as={Link} to="/pooling">
                                    {t('menu.pooling')}
                                </BreadcrumbLink>
                            </BreadcrumbItem>

                            <BreadcrumbItem>
                                <BreadcrumbLink
                                    as={Link}
                                    to={`/materials/${material._id}`}
                                >
                                    {material.name}
                                </BreadcrumbLink>
                            </BreadcrumbItem>

                            <BreadcrumbItem isCurrentPage isLastChild>
                                <BreadcrumbLink>
                                    {t('pooling.edit.breadcrumb')}
                                </BreadcrumbLink>
                            </BreadcrumbItem>
                        </Breadcrumb>
                        <SlideFade in={!loading} offsetY="300px">
                            <MaterialForm
                                initialStep={step ?? 'information'}
                                material={material}
                            />
                        </SlideFade>
                    </Container>
                ))}
        </>
    )
}

export default MaterialEdit
