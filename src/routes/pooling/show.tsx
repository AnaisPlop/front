import * as React from 'react'
import {
    Box,
    VStack,
    Text,
    Heading,
    SimpleGrid,
    Container,
    Flex,
    GridItem,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    useDisclosure,
    Spacer,
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
} from '@chakra-ui/react'
import { useQuery } from '@apollo/client'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { useEffect, useState } from 'react'
import { FiHome } from 'react-icons/fi'
import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import { Material, MaterialBooking } from '../../types'
import { findMaterialById } from '../../repositories/Material/MaterialRepository'
import UserCard from '../../components/molecules/Cards/UserCard'
import 'react-medium-image-zoom/dist/styles.css'
import AvailabilityPlanning from '../../components/molecules/Form/Material/Booking/AvailabilityPlanning'
import BookingContext from '../../contexts/BookingContext'
import useAuth from '../../auth/useAuth'
import ExchangeMailCloud from '../../components/atoms/Illustrations/ExchangeMailCloud'
import Pictures from '../../components/molecules/Material/Pictures'
import EditButton from '../../components/molecules/Material/EditButton'
import BookButton from '../../components/molecules/Material/BookButton'
import Loader from '../../components/atoms/Loader/Loader'
import moneyFormat from '../../utils/money'
import randomTrans from '../../utils/randomTrans'

const MaterialShow = () => {
    const { t, i18n } = useTranslation()
    const [step, setStep] = useState<'initial' | 'choosePeriod' | 'pending'>(
        'initial'
    )
    const [booking, setBooking] = useState<MaterialBooking | null>()
    const params = useParams()
    const auth = useAuth()
    const navigate = useNavigate()
    const { loading, error, data } = useQuery(findMaterialById, {
        variables: {
            id: `/materials/${params.id}`,
        },
    })
    let material: Material | null = null
    if (!loading && data) {
        material = data.material
    }
    const {
        isOpen: isValidationOpen,
        onOpen: onValidationOpen,
        onClose: onValidationClose,
    } = useDisclosure()
    useEffect(() => {
        if (booking && booking.status === 'pending') {
            onValidationOpen()
        }
    }, [booking, onValidationOpen])

    return (
        (error && <p>Error </p>) ||
        (loading && <Loader />) ||
        (material && (
            // eslint-disable-next-line react/jsx-no-constructed-context-values
            <BookingContext.Provider value={{ booking, setBooking }}>
                <Helmet>
                    <title>
                        {material.name} {material.brand} {material.model}{' '}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>

                <Container maxW="7xl" minW="300px">
                    <Breadcrumb py={2} color="yellow.600">
                        <BreadcrumbItem>
                            <BreadcrumbLink as={Link} to="/">
                                <FiHome />
                            </BreadcrumbLink>
                        </BreadcrumbItem>

                        <BreadcrumbItem>
                            <BreadcrumbLink as={Link} to="/pooling/search">
                                {t('menu.pooling')}
                            </BreadcrumbLink>
                        </BreadcrumbItem>

                        <BreadcrumbItem isCurrentPage isLastChild>
                            <BreadcrumbLink>{material.name}</BreadcrumbLink>
                        </BreadcrumbItem>
                    </Breadcrumb>
                    <Flex direction="column">
                        <SimpleGrid
                            display={{
                                base: 'block',
                                md: 'grid',
                            }}
                            order={{ base: 2, md: 1 }}
                            columns={
                                step === 'initial' || step === 'pending'
                                    ? { base: 1, md: 5 }
                                    : { base: 1, md: 6 }
                            }
                            spacing={{ base: 8, md: 10 }}
                            pt={{ base: 9, md: 15 }}
                            gap={8}
                        >
                            <GridItem
                                rowSpan={{ base: 1, md: 2 }}
                                colSpan={
                                    step === 'initial' || step === 'pending'
                                        ? { base: 1, md: 3 }
                                        : { base: 1, md: 2 }
                                }
                                display={
                                    step === 'choosePeriod'
                                        ? { base: 'none', md: 'grid' }
                                        : 'inherit'
                                }
                            >
                                <Pictures material={material} />
                                {step === 'choosePeriod' && (
                                    <UserCard
                                        user={material.owner}
                                        borderRadius={
                                            step === 'choosePeriod'
                                                ? '0 10px 10px 0'
                                                : undefined
                                        }
                                    />
                                )}
                            </GridItem>
                            <GridItem
                                rowSpan={2}
                                colSpan={
                                    step === 'initial' || step === 'pending'
                                        ? 2
                                        : 4
                                }
                            >
                                {step === 'choosePeriod' && (
                                    <Flex height="100%">
                                        <AvailabilityPlanning
                                            material={material}
                                            onBack={() => setStep('initial')}
                                            setStep={setStep}
                                        />
                                    </Flex>
                                )}
                                {(step === 'initial' || step === 'pending') && (
                                    <VStack
                                        display={{
                                            base: 'none',
                                            md: 'block',
                                        }}
                                    >
                                        <Box w="100%">
                                            <UserCard user={material.owner} />
                                        </Box>
                                        <Box width="100%">
                                            {(material.owner._id ===
                                                auth.user?._id && (
                                                <EditButton
                                                    material={material}
                                                />
                                            )) || (
                                                <BookButton
                                                    material={material}
                                                    onClick={() =>
                                                        auth.user
                                                            ? setStep(
                                                                  'choosePeriod'
                                                              )
                                                            : navigate('/login')
                                                    }
                                                />
                                            )}
                                        </Box>
                                    </VStack>
                                )}
                            </GridItem>
                        </SimpleGrid>
                        <Heading
                            order={{ base: 1, md: 1 }}
                            lineHeight={1.1}
                            fontWeight={600}
                            mt={30}
                            fontSize={{
                                base: '2xl',
                                sm: '4xl',
                                lg: '5xl',
                            }}
                        >
                            {material.name}
                        </Heading>
                        <Box order={{ base: 3, md: 1 }}>
                            {material.pricings && (
                                    <Text
                                        colorScheme="gray"
                                        fontWeight={700}
                                        as="span"
                                        mr={2}
                                        fontSize="lg"
                                    >
                                        {t('pooling.show.pricing.label')}
                                    </Text>
                                ) &&
                                material.pricings.collection.map((pricing) => (
                                    <Text
                                        key={pricing._id}
                                        colorScheme="gray"
                                        fontWeight={300}
                                        as="span"
                                        borderBottomWidth={2}
                                        borderBottomStyle="solid"
                                        borderBottomColor="red"
                                        mr={3}
                                        fontSize="lg"
                                    >
                                        {pricing.value === 0
                                            ? randomTrans(
                                                  t,
                                                  'pooling.show.pricing.free',
                                                  9
                                              )
                                            : t('pooling.show.pricing.price', {
                                                  amount: moneyFormat(
                                                      pricing.value,
                                                      i18n.language
                                                  ),
                                                  period: t(
                                                      `pooling.show.pricing.period.${pricing.period}`
                                                  ),
                                              }) +
                                              (pricing.circles.collection
                                                  .length > 0
                                                  ? ` (${pricing.circles.collection.map(
                                                        (circle) => circle.name
                                                    )})`
                                                  : '')}
                                    </Text>
                                ))}
                        </Box>
                        <Box
                            order={{ base: 4, md: 1 }}
                            whiteSpace="pre-line"
                            my={5}
                        >
                            {material.description}
                        </Box>
                        <Box
                            order={{ base: 5, md: 1 }}
                            display={{
                                base: 'block',
                                md: 'none',
                            }}
                        >
                            <VStack>
                                <Box width="100%">
                                    {(material.owner._id === auth.user?._id && (
                                        <EditButton material={material} />
                                    )) || (
                                        <BookButton
                                            material={material}
                                            onClick={() =>
                                                setStep('choosePeriod')
                                            }
                                        />
                                    )}
                                </Box>
                                <UserCard
                                    user={material.owner}
                                    borderRadius={
                                        step === 'choosePeriod'
                                            ? '0 10px 10px 0'
                                            : undefined
                                    }
                                />
                            </VStack>
                        </Box>
                    </Flex>
                </Container>
                <Modal
                    motionPreset="slideInBottom"
                    onClose={onValidationClose}
                    isOpen={isValidationOpen}
                    isCentered
                >
                    <ModalOverlay />
                    <ModalContent pb={5}>
                        <ModalHeader>
                            {t('pooling.show.booking.validate.success.title')}
                        </ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Box whiteSpace="pre-line">
                                <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                                    {t(
                                        'pooling.show.booking.validate.success.body',
                                        {
                                            user: material.owner,
                                            context: material.owner.gender,
                                        }
                                    )}
                                </ReactMarkdown>
                            </Box>
                            <ExchangeMailCloud
                                height={{ sm: '16rem', lg: '24rem' }}
                                mt={{ base: 12, sm: 16 }}
                            />
                            <Spacer h={35} />
                        </ModalBody>
                    </ModalContent>
                </Modal>
            </BookingContext.Provider>
        ))
    )
}

export default MaterialShow
