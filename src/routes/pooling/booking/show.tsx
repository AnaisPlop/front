import * as React from 'react'
import {
    Container,
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
    Flex,
    useColorModeValue,
    Stack,
    SimpleGrid,
    Box,
    Input,
    FormControl,
    Button,
    InputRightElement,
    InputGroup,
    Spacer,
} from '@chakra-ui/react'
import { useApolloClient, useMutation, useQuery } from '@apollo/client'
import { Link, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import 'react-medium-image-zoom/dist/styles.css'
import 'react-chat-elements/dist/main.css'
import { FiHome } from 'react-icons/fi'
import moment from 'moment'
import { FaRegPaperPlane } from 'react-icons/fa'
import { FieldValues, useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import { findMaterialBookingById } from '../../../repositories/Material/MaterialBookingRepository'
import Loader from '../../../components/atoms/Loader/Loader'
import { MaterialBooking, User } from '../../../types'
import useAuth from '../../../auth/useAuth'
import BookingStatusActionButton from '../../../components/atoms/Button/material/booking/BookingStatusActionButton'
import BookingPriceStat from '../../../components/atoms/Stat/Material/Booking/BookingPriceStat'
import { H1, H2 } from '../../../components/atoms/Heading'
import MaterialHorizontalCard from '../../../components/molecules/Cards/MaterialHorizontalCard'
import Messages from '../../../components/molecules/Message/Messages'
import { addMessageDiscussion } from '../../../repositories/Discussion/DiscussionRepository'

const MaterialBookingShow = () => {
    const { t } = useTranslation()
    const auth = useAuth()
    const params = useParams()
    const {
        loading,
        error,
        data: materialBookingData,
    } = useQuery(findMaterialBookingById, {
        variables: {
            id: `/material_bookings/${params.id}`,
        },
    })
    let booking: MaterialBooking | null = null
    let user: User | null = null
    if (!loading) {
        if (materialBookingData.materialBooking) {
            booking = materialBookingData.materialBooking
            user =
                auth.user?.id === materialBookingData.materialBooking.id
                    ? materialBookingData.materialBooking.material.owner
                    : materialBookingData.materialBooking.user
        }
    }
    const bg = useColorModeValue('white', 'gray.600')
    const bgAccent = useColorModeValue('gray.100', 'gray.800')

    const client = useApolloClient()
    const [addMessage] = useMutation(addMessageDiscussion, {
        update() {
            client.resetStore()
        },
    })
    const { handleSubmit, register, resetField } = useForm<FieldValues>()

    const onSubmit = (data: FieldValues) => {
        addMessage({
            variables: {
                id: data.id,
                message: data.message,
            },
        }).then((r) => {
            if (r.data.addMessageDiscussion.discussion === null) {
                toast.error(t(``))
                return
            }
            resetField('message')
        })
    }

    return (
        (error && <p>Error </p>) ||
        (loading && <Loader />) ||
        (booking && (
            <>
                <Helmet>
                    <title>
                        {booking._id}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>

                <Container maxW="7xl" minW="300px">
                    <Breadcrumb py={2} color="yellow.600">
                        <BreadcrumbItem>
                            <BreadcrumbLink as={Link} to="/">
                                <FiHome />
                            </BreadcrumbLink>
                        </BreadcrumbItem>

                        <BreadcrumbItem>
                            <BreadcrumbLink as={Link} to="/my/bookings">
                                {t('menu.my.bookings')}
                            </BreadcrumbLink>
                        </BreadcrumbItem>

                        <BreadcrumbItem isCurrentPage isLastChild>
                            <BreadcrumbLink>
                                {t('menu.my.booking', {
                                    booking,
                                    dateOfCreation: moment(
                                        booking.createdAt
                                    ).format('L'),
                                    user,
                                })}
                            </BreadcrumbLink>
                        </BreadcrumbItem>
                    </Breadcrumb>

                    <Stack
                        bg={bg}
                        rounded="xl"
                        p={{ base: 4, sm: 6, md: 8 }}
                        my={{ base: 4, sm: 6, md: 8 }}
                        w="100%"
                        gap="10"
                    >
                        <Flex>
                            <H1>
                                {t('pooling.booking.show.title', {
                                    booking,
                                    dateOfCreation: moment(
                                        booking.createdAt
                                    ).format('L'),
                                    user,
                                })}
                            </H1>
                            <Spacer />
                            <div>
                                <Box
                                    px={8}
                                    py={5}
                                    borderRadius="xl"
                                    bg={bgAccent}
                                >
                                    <H2 m={0}>
                                        {t('pooling.booking.status', {
                                            context: booking.status,
                                        })}
                                    </H2>
                                    {booking?.statusTransitionAvailables
                                        .length > 0 && (
                                        <Box mt={3}>
                                            <BookingStatusActionButton
                                                _booking={booking}
                                            />
                                        </Box>
                                    )}
                                </Box>
                            </div>
                        </Flex>
                        <SimpleGrid columns={{ base: 1, sm: 2 }} spacing={10}>
                            <Box>
                                <MaterialHorizontalCard
                                    material={booking.material}
                                />
                                <Flex direction="column" gap={10}>
                                    <BookingPriceStat
                                        price={booking.price || 0}
                                        periods={booking.periods.edges.map(
                                            ({ node }) => {
                                                const {
                                                    _id,
                                                    startDate,
                                                    endDate,
                                                } = node
                                                return {
                                                    _id,
                                                    startDate,
                                                    endDate,
                                                }
                                            }
                                        )}
                                    />
                                </Flex>
                            </Box>
                            <Box>
                                <Messages
                                    messages={booking.discussion.messages.edges.map(
                                        ({ node }) => node
                                    )}
                                />
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <input
                                        type="hidden"
                                        {...register('id')}
                                        value={booking.discussion.id}
                                    />
                                    <FormControl>
                                        <InputGroup size="md">
                                            <Input
                                                type="text"
                                                placeholder="message"
                                                {...register('message')}
                                            />
                                            <InputRightElement width="3.5rem">
                                                <Button
                                                    h="1.75rem"
                                                    size="sm"
                                                    variant="ghost"
                                                    type="submit"
                                                >
                                                    <FaRegPaperPlane />
                                                </Button>
                                            </InputRightElement>
                                        </InputGroup>
                                    </FormControl>
                                </form>
                            </Box>
                            <Box>
                                {/* {booking.discussion.messages.edges.map( */}
                                {/*    ({ node: message }) => { */}
                                {/*        if (message.author) { */}
                                {/*            return ( */}
                                {/*                <MessageBox */}
                                {/*                    key={message.id} */}
                                {/*                    // @ts-ignore */}
                                {/*                    avatar={`${process.env.REACT_APP_API_URL}${message.author?.avatar?.contentUrl}`} */}
                                {/*                    date={message.createdAt} */}
                                {/*                    position={ */}
                                {/*                        message.author.id === */}
                                {/*                        user?.id */}
                                {/*                            ? 'right' */}
                                {/*                            : 'left' */}
                                {/*                    } */}
                                {/*                    type="text" */}
                                {/*                    text={ */}
                                {/*                        <ReactMarkdown */}
                                {/*                            rehypePlugins={[ */}
                                {/*                                rehypeRaw, */}
                                {/*                            ]} */}
                                {/*                        > */}
                                {/*                            {message.content} */}
                                {/*                        </ReactMarkdown> */}
                                {/*                    } */}
                                {/*                    title={ */}
                                {/*                        message.author && */}
                                {/*                        message.author?.fullname */}
                                {/*                    } */}
                                {/*                /> */}
                                {/*            ) */}
                                {/*        } */}
                                {/*        return ( */}
                                {/*            <MessageBox */}
                                {/*                key={message.id} */}
                                {/*                // @ts-ignore */}
                                {/*                type="system" */}
                                {/*                text={ */}
                                {/*                    <ReactMarkdown */}
                                {/*                        rehypePlugins={[ */}
                                {/*                            rehypeRaw, */}
                                {/*                        ]} */}
                                {/*                    > */}
                                {/*                        {message.content} */}
                                {/*                    </ReactMarkdown> */}
                                {/*                } */}
                                {/*            /> */}
                                {/*        ) */}
                                {/*    } */}
                                {/* )} */}
                            </Box>
                        </SimpleGrid>
                    </Stack>
                    <Flex direction="column" gap={5} my={5}>
                        <Flex gap={5}>
                            <Flex direction="column" gap={5} />
                            <Flex>
                                {/* <HStack
                                    marginTop="2"
                                    spacing="2"
                                    display="flex"
                                    direction="row"
                                    alignItems="center"
                                >
                                    <Avatar
                                        size="sm"
                                        src={`${process.env.REACT_APP_API_URL}${message.author.avatar?.contentUrl}`}
                                        name={`${message.author.firstname} ${message.author.lastname}`}
                                        mb={4}
                                        pos="relative"
                                    />
                                    <Text fontWeight="medium">
                                        {message.author.firstname} —
                                    </Text>
                                    <Text>
                                        {moment(
                                            message.createdAt
                                        ).format('LLLL')}
                                    </Text>
                                </HStack>
                                <Card>{message.content}</Card> */}
                            </Flex>
                        </Flex>
                    </Flex>
                </Container>
            </>
        ))
    )
}

export default MaterialBookingShow
