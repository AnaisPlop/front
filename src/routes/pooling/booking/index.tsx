import * as React from 'react'
import {
    Box,
    Heading,
    SlideFade,
    Container,
    Tabs,
    TabList,
    Tab,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { useQuery } from '@apollo/client'
import { useState } from 'react'
import {
    findMaterialBookingsForApplicant,
    findMaterialBookingsForOwner,
} from '../../../repositories/Material/MaterialBookingRepository'
import Loader from '../../../components/atoms/Loader/Loader'
import 'moment/locale/fr'
import ProfileSidebar from '../../../components/molecules/User/Profile/ProfileSidebar'
import useAuth from '../../../auth/useAuth'
import BookingTable from '../../../components/molecules/Table/Material/Booking/BookingTable'

const MaterialBookingIndex = () => {
    const { t } = useTranslation()
    const auth = useAuth()
    const [userSide, setUserSide] = useState<'user' | 'owner'>('user')
    let query = findMaterialBookingsForApplicant
    const variables = {
        id: auth.user?.id,
        statuses: ['pending', 'confirmed', 'closed'],
    }
    if (userSide === 'owner') {
        query = findMaterialBookingsForOwner
    }
    const { loading, error, data } = useQuery(query, {
        variables,
    })
    let materialBookings = []
    if (!loading) {
        materialBookings = data.materialBookings.edges
    }
    return (
        (loading && <Loader />) ||
        (error && <div>Error :(</div>) || (
            <>
                <Helmet>
                    <title>
                        {t('pooling.booking.index.title')}{' '}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>
                <Container maxW="7xl" minW="300px">
                    <Box textAlign="center" fontSize="xl" w="100%">
                        <SlideFade in={!loading} offsetY="300px">
                            <ProfileSidebar>
                                <Heading>
                                    {t('pooling.booking.index.title')}
                                </Heading>
                                <Tabs
                                    defaultIndex={userSide === 'user' ? 0 : 1}
                                >
                                    <TabList>
                                        <Tab
                                            onClick={() => setUserSide('user')}
                                        >
                                            {t('pooling.booking.index.title', {
                                                context: 'my_requests',
                                            })}
                                        </Tab>
                                        <Tab
                                            onClick={() => setUserSide('owner')}
                                        >
                                            {t('pooling.booking.index.title', {
                                                context: 'as_owner',
                                            })}
                                        </Tab>
                                    </TabList>
                                </Tabs>
                                <BookingTable
                                    materialBookings={materialBookings}
                                />
                            </ProfileSidebar>
                        </SlideFade>
                    </Box>
                </Container>
            </>
        )
    )
}

export default MaterialBookingIndex
