import React from 'react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { Container } from '@chakra-ui/react'
import MaterialForm from '../../components/molecules/Form/Material'

const MaterialNew = () => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>
                    {t('pooling.new.title')} {'< Communo'}
                </title>
            </Helmet>
            <Container maxW="7xl" minW="300px">
                <MaterialForm initialStep="information" />
            </Container>
        </>
    )
}

export default MaterialNew
