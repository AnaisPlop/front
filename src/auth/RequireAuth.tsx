import React, { ReactElement } from 'react'
import { useLocation, Navigate } from 'react-router-dom'
import { useCookies } from 'react-cookie'

const RequireAuth = ({ children }: { children: ReactElement }) => {
    const location = useLocation()
    const [cookies, , removeCookie] = useCookies()
    if (!localStorage.getItem('token') || cookies.user === undefined) {
        removeCookie('user')
        return <Navigate to="/login" state={{ from: location }} replace />
    }

    return children
}

export default RequireAuth
