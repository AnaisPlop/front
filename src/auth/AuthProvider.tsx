import React, {
    createContext,
    Dispatch,
    ReactNode,
    useEffect,
    useMemo,
    useState,
} from 'react'
import { useCookies } from 'react-cookie'
import { CookieSetOptions } from 'universal-cookie'
import { SimpleUser, User } from '../types'

interface JWTAuthResponse {
    user: User
    token: string
    tokenExpiresAt: string
    refreshToken: string
}
interface AuthContextType {
    user: SimpleUser | null
    token: string | null
    signin: (response: JWTAuthResponse, callback: VoidFunction) => void
    signinGQL: (response: User, callback: VoidFunction) => void
    signout: (callback: VoidFunction) => void
}

export const AuthContext = createContext<AuthContextType>({
    user: null,
    token: null,
    signin: () => {},
    signinGQL: () => {},
    signout: () => {},
})

const computeAuthContextValue = (
    user: User | null,
    token: string | null,
    refreshToken: string | null,
    tokenExpiresAt: string | null,
    signin: (response: JWTAuthResponse, callback: VoidFunction) => void,
    signout: (callback: VoidFunction) => void,
    signinGQL: (response: User, callback: VoidFunction) => void
) => {
    return {
        user,
        token,
        refreshToken,
        tokenExpiresAt,
        signin,
        signout,
        signinGQL,
    }
}

const doSignout = (
    removeCookie: (
        name: string,
        options?: CookieSetOptions | undefined
    ) => void,
    setToken: Dispatch<string | null>,
    setTokenExpiresAt: Dispatch<string | null>,
    setRefreshToken: Dispatch<string | null>,
    setUser: Dispatch<User | null>
) => {
    return (callback: VoidFunction) => {
        removeCookie('user')
        setToken(null)
        setTokenExpiresAt(null)
        setRefreshToken(null)
        setUser(null)
        localStorage.removeItem('token')
        localStorage.removeItem('tokenExpiresAt')
        localStorage.removeItem('refreshToken')
        callback()
    }
}

function doSigninGQL(
    setCookie: (
        name: string,
        value: any,
        options?: CookieSetOptions | undefined
    ) => void,
    setToken: Dispatch<string | null>,
    setTokenExpiresAt: Dispatch<string | null>,
    setRefreshToken: Dispatch<string | null>,
    setUser: Dispatch<User | null>
) {
    return (response: User, callback: VoidFunction) => {
        setUser(response)
        setToken(response.token)
        setTokenExpiresAt(response.tokenExpiresAt)
        setRefreshToken(response.refreshToken)
        localStorage.setItem('token', response.token ?? '')
        localStorage.setItem('tokenExpiresAt', response.tokenExpiresAt ?? '')
        localStorage.setItem('refreshToken', response.refreshToken ?? '')
        setCookie('user', response)
        callback()
    }
}

function doSignin(
    setCookie: (
        name: string,
        value: any,
        options?: CookieSetOptions | undefined
    ) => void,
    setToken: Dispatch<string | null>,
    setTokenExpiresAt: Dispatch<string | null>,
    setRefreshToken: Dispatch<string | null>,
    setUser: Dispatch<User | null>
) {
    return (response: JWTAuthResponse, callback: VoidFunction) => {
        setToken(response.token)
        setUser(response.user)
        setTokenExpiresAt(response.tokenExpiresAt)
        setRefreshToken(response.refreshToken)
        localStorage.setItem('token', response.token)
        localStorage.setItem('tokenExpiresAt', response.tokenExpiresAt)
        localStorage.setItem('refreshToken', response.refreshToken)
        setCookie('user', response.user)
        callback()
    }
}

export const AuthProvider = ({ children }: { children: ReactNode }) => {
    const [user, setUser] = useState<User | null>(null)
    const [token, setToken] = useState<string | null>(null)
    const [tokenExpiresAt, setTokenExpiresAt] = useState<string | null>(null)
    const [refreshToken, setRefreshToken] = useState<string | null>(null)
    const [cookies, setCookie, removeCookie] = useCookies()

    const cookieUser = cookies.user
    useEffect(() => {
        const tmpToken = localStorage.getItem('token')
        const tmpRefreshToken = localStorage.getItem('refreshToken')
        if (tmpToken !== undefined) {
            setToken(tmpToken)
        }
        if (tmpRefreshToken !== undefined) {
            setRefreshToken(tmpRefreshToken)
        }
        if (cookieUser !== undefined) {
            setUser(cookieUser)
        }
    }, [cookieUser])

    const signin = useMemo(
        () =>
            doSignin(
                setCookie,
                setToken,
                setTokenExpiresAt,
                setRefreshToken,
                setUser
            ),
        [setCookie, setToken, setTokenExpiresAt, setRefreshToken, setUser]
    )

    /**
     * signinGQL is a way to log in thanks graphql mutation
     * @param response
     * @param callback
     */
    const signinGQL = useMemo(
        () =>
            doSigninGQL(
                setCookie,
                setToken,
                setTokenExpiresAt,
                setRefreshToken,
                setUser
            ),
        [setCookie, setToken, setTokenExpiresAt, setRefreshToken, setUser]
    )

    const signout = useMemo(
        () =>
            doSignout(
                removeCookie,
                setToken,
                setTokenExpiresAt,
                setRefreshToken,
                setUser
            ),
        [removeCookie, setToken, setTokenExpiresAt, setRefreshToken, setUser]
    )

    const value = useMemo(
        () =>
            computeAuthContextValue(
                user,
                token,
                refreshToken,
                tokenExpiresAt,
                signin,
                signout,
                signinGQL
            ),
        [user, token, refreshToken, tokenExpiresAt, signin, signout, signinGQL]
    )

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}
