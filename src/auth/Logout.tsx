import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import useAuth from './useAuth'

const Logout = () => {
    const navigate = useNavigate()
    const auth = useAuth()
    useEffect(() => {
        auth.signout(() => {
            navigate('/', { replace: true })
        })
    })

    return <div />
}

export default Logout
