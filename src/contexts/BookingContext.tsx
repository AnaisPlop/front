import * as React from 'react'
import { Dispatch, SetStateAction } from 'react'
import { MaterialBooking } from '../types'

interface BookingContextInterface {
    booking: MaterialBooking | null | undefined
    setBooking: Dispatch<SetStateAction<MaterialBooking | null | undefined>>
}

const BookingContext = React.createContext<BookingContextInterface | null>(null)

export default BookingContext
