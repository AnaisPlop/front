import { TFunction } from 'i18next'

const randomTrans = (
    t: TFunction,
    translationString: string,
    itemCount: number,
    params: object = {}
) => t(`${translationString}.${Math.floor(Math.random() * itemCount)}`, params)

export default randomTrans
