const moneyFormat = (amount: number, locale: string) => {
    const moneyFormatter = new Intl.NumberFormat(locale, {
        style: 'currency',
        currency: 'EUR',
    })
    return moneyFormatter.format(amount)
}

export default moneyFormat
