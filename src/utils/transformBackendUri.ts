const transformBackendUri = (uri?: string): string => {
    if (!uri) {
        return ''
    }
    if (uri.match(/^https?:\/\//)) {
        return uri
    }

    return `${process.env.REACT_APP_API_URL}${uri}`
}
export default transformBackendUri
