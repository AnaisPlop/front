import React from 'react'
import {
    Box,
    useColorModeValue,
    useDisclosure,
    useColorMode,
} from '@chakra-ui/react'
import { FaTools } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { ToastContainer } from 'react-toastify'
import { Outlet } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import { BiAnalyse } from 'react-icons/bi'
import Footer from '../molecules/Footer/Footer'
import SideBar from '../molecules/Menu/SideBar'
import { LinkItemProps } from './LayoutProps'
import HeadBar from '../molecules/Menu/HeadBar'

const Layout = () => {
    const { t } = useTranslation()
    const MainMenu: Array<LinkItemProps> = [
        {
            name: t('menu.project'),
            icon: BiAnalyse,
            display: { base: 'none', md: 'flex' },
            subItems: [
                {
                    name: t('menu.why'),
                    to: '/why',
                    display: { base: 'none', md: 'flex' },
                },
                {
                    name: t('menu.contribute'),
                    to: '/contribute',
                    display: { base: 'none', md: 'flex' },
                },
                {
                    name: t('menu.donate'),
                    to: '/donate',
                    display: { base: 'none', md: 'flex' },
                },
                // {
                //     name: t('menu.next'),
                //     to: '/next',
                //     display: { base: 'none', md: 'flex' },
                // },
            ],
        },
        {
            name: t('menu.pooling'),
            icon: FaTools,
            to: '/pooling',
            display: { base: 'none', md: 'flex' },
        },
    ]
    const { onOpen, onClose, isOpen } = useDisclosure()
    const { colorMode } = useColorMode()

    return (
        <>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Communo</title>
                <link
                    rel="icon"
                    type="image/svg+xml"
                    href={useColorModeValue(
                        '/favicon-light.svg',
                        '/favicon.svg'
                    )}
                />
            </Helmet>
            <Box minH="100vh" bg={useColorModeValue('gray.100', 'gray.700')}>
                <HeadBar onOpen={onOpen} links={MainMenu} />
                <SideBar
                    onClose={onClose}
                    linkItems={MainMenu}
                    isOpen={isOpen}
                />
                <Outlet />
                <ToastContainer
                    theme={colorMode}
                    draggablePercent={60}
                    role="alert"
                />
                <Footer />
            </Box>
        </>
    )
}

export default Layout
