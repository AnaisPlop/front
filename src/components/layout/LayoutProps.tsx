import { IconType } from 'react-icons'
import { LayoutProps } from '@chakra-ui/styled-system'

export declare type SubLinkItemProps = {
    name: string
    icon?: IconType
    to?: string
} & LayoutProps

export declare type LinkItemProps = {
    subItems?: SubLinkItemProps[]
} & SubLinkItemProps
