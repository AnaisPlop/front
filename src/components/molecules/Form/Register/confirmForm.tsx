import {
    Box,
    useColorModeValue,
    HStack,
    PinInput,
    PinInputField,
} from '@chakra-ui/react'
import React, { Dispatch, SetStateAction } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import { useApolloClient, useMutation } from '@apollo/client'
import { useNavigate } from 'react-router-dom'
import { confirmUser } from '../../../../repositories/User/UserRepository'
import useAuth from '../../../../auth/useAuth'
import randomTrans from '../../../../utils/randomTrans'

const ConfirmForm = ({
    confirm,
    userId,
}: {
    confirm: Dispatch<SetStateAction<boolean>>
    userId: string
}) => {
    const { t } = useTranslation()
    const client = useApolloClient()
    const [confirmCode] = useMutation(confirmUser, {
        update() {
            client.resetStore()
        },
    })
    const auth = useAuth()
    const navigate = useNavigate()
    const onComplete = (code: string) => {
        confirmCode({
            variables: {
                code,
                id: `/users/${userId}`,
            },
        }).then((r) => {
            if (r.data.confirmUser.user === null) {
                toast.error(t('register.toast.confirm.error'))
                return
            }
            confirm(true)
            auth.signinGQL(r.data.confirmUser.user, () => {
                navigate('/', { replace: true })
            })
            toast(
                randomTrans(t, 'register.toast.success', 4, {
                    user: r.data.confirmUser.user,
                })
            )
        })
    }

    return (
        <Box
            rounded="lg"
            bg={useColorModeValue('white', 'gray.700')}
            boxShadow="lg"
            p={8}
        >
            <HStack spacing={4}>
                <PinInput
                    type="alphanumeric"
                    size="lg"
                    otp
                    onComplete={(e) => {
                        onComplete(e)
                    }}
                >
                    <PinInputField autoFocus />
                    <PinInputField />
                    <PinInputField />
                    <PinInputField />
                </PinInput>
            </HStack>
        </Box>
    )
}

export default ConfirmForm
