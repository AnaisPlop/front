import React, { useState } from 'react'
import {
    Box,
    Stack,
    Input,
    Button,
    SimpleGrid,
    Avatar,
    FormErrorMessage,
    FormControl,
    FormLabel,
    InputGroup,
    InputRightElement,
    Center,
    AvatarBadge,
    IconButton,
    FormHelperText,
    Tooltip,
    Textarea,
    Wrap,
} from '@chakra-ui/react'
import { FieldValues, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { FaInfo, FaRegEye, FaRegEyeSlash, FaTrashAlt } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import CountryForm from '../../../atoms/Form/CountryForm'
import GenderForm from '../../../atoms/Form/GenderForm'
import 'react-phone-number-input/style.css'
import RequiredAsterisk from '../../../atoms/Form/RequiredAsterisk'
import PhoneNumberInput from '../../../atoms/Form/PhoneNumberInput'
import { PrimaryButton } from '../../../atoms/Button'
import 'yup-phone'

const RegisterForm = () => {
    const { t, i18n } = useTranslation()
    const [showPassword, setShowPassword] = useState(false)
    const [avatar, setAvatar] = useState('')
    const schema = yup.object().shape({
        email: yup.string().email().required(),
        password: yup.string().min(8).max(32).required(),
        city: yup.string().required(),
        firstname: yup.string().required(),
        lastname: yup.string().required(),
        streetAddress: yup.string().required(),
    })
    const {
        handleSubmit,
        register,
        formState: { errors, isSubmitting },
        setError,
    } = useForm<FieldValues>({
        defaultValues: {
            language: i18n.language,
            country: i18n.language === 'en' ? 'EN' : 'FR',
            countryCode: i18n.language === 'en' ? 1 : 33,
        },
        resolver: yupResolver(schema),
    })

    const navigate = useNavigate()

    const onSubmit = (values: FieldValues) => {
        const formData = new FormData()
        // eslint-disable-next-line guard-for-in,no-restricted-syntax
        for (const key in values) {
            if (key === 'avatar') {
                formData.append('avatar', values.avatar[0])
            } else if (key === 'phoneNumberObject') {
                formData.append(
                    'nationalNumber',
                    values.phoneNumberObject.nationalNumber
                )
                formData.append(
                    'countryCode',
                    values.phoneNumberObject.countryCode
                )
            } else {
                formData.append(key, values[key])
            }
        }
        const requestOptions = {
            method: 'POST',
            body: formData,
            headers: {
                'Accept-Language': i18n.language,
            },
        }
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, requestOptions)
            .then((response) => response.json())
            .then((response) => {
                if (
                    ['hydra:Error', 'ConstraintViolationList'].includes(
                        response['@type']
                    )
                ) {
                    response.violations.map(
                        (error: { propertyPath: string; message: string }) =>
                            setError(error.propertyPath, {
                                type: 'custom',
                                message: error.message,
                            })
                    )
                    throw new Error(response['hydra:description'])
                }
                toast(t('register.toast.confirm'))
                navigate(`/register/confirm/${response.id}`)
            })
            .catch((error) => {
                const errorMessage = t(
                    `register.toast.error.${Math.floor(Math.random() * 5)}`,
                    { error: error.toString().replace(/^Error: /, '') }
                )
                toast.error(errorMessage)
            })
    }

    // @ts-ignore
    // @ts-ignore
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input type="hidden" {...register('language')} />
            <FormErrorMessage>
                {errors.password && <p>Oups</p>}
            </FormErrorMessage>
            <SimpleGrid columns={{ base: 1, sm: 2 }} spacingX={10} spacingY={3}>
                <Box>
                    <FormControl id="userName">
                        <FormLabel>
                            <Stack direction={['column', 'row']} spacing={6}>
                                <Center>
                                    <Avatar
                                        size="xl"
                                        src={avatar}
                                        cursor="pointer"
                                    >
                                        {avatar && (
                                            <AvatarBadge
                                                as={IconButton}
                                                size="sm"
                                                rounded="full"
                                                top="-10px"
                                                colorScheme="red"
                                                color="white"
                                                aria-label="remove Image"
                                                onClick={() => {
                                                    setAvatar('')
                                                }}
                                                icon={<FaTrashAlt />}
                                            />
                                        )}
                                    </Avatar>
                                </Center>
                                <Center w="full">
                                    <Input
                                        type="file"
                                        w="full"
                                        {...register('avatar')}
                                        onChange={(e) => {
                                            setAvatar(
                                                (e.target.files &&
                                                    URL.createObjectURL(
                                                        e.target.files[0]
                                                    )) ||
                                                    ''
                                            )
                                        }}
                                    />
                                </Center>
                            </Stack>
                        </FormLabel>
                    </FormControl>
                </Box>
                <Box alignSelf="end">
                    <FormControl
                        id="email"
                        isInvalid={errors.email !== undefined}
                    >
                        <FormLabel>
                            <>
                                {t('register.form.email.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        </FormLabel>
                        <Input
                            type="email"
                            placeholder={t('register.form.email.placeholder')}
                            isRequired
                            {...register('email', {
                                required: t('form.required').toString(),
                                minLength: {
                                    value: 4,
                                    message: 'Minimum length should be 4',
                                },
                                pattern: /^\S+@\S+$/i,
                            })}
                        />
                        {errors.email !== undefined && (
                            <FormErrorMessage>
                                {errors.email.message}
                            </FormErrorMessage>
                        )}
                    </FormControl>
                </Box>
            </SimpleGrid>
            <SimpleGrid columns={{ base: 1, sm: 3 }} spacingX={10} spacingY={3}>
                <Box>
                    <FormControl
                        id="gender"
                        isInvalid={errors.gender !== undefined}
                    >
                        <FormLabel>{t('register.form.gender.label')}</FormLabel>
                        <GenderForm
                            isRequired={false}
                            register={register('gender')}
                        />
                        <FormHelperText>
                            <Tooltip
                                hasArrow
                                label={t('register.form.gender.help')}
                            >
                                <Button
                                    variant="link"
                                    size="sm"
                                    leftIcon={<FaInfo />}
                                >
                                    {t('register.form.gender.why')}
                                </Button>
                            </Tooltip>
                        </FormHelperText>
                    </FormControl>
                </Box>
                <Box>
                    <FormControl
                        id="firstname"
                        isInvalid={errors.firstname !== undefined}
                    >
                        <FormLabel>
                            <>
                                {t('register.form.firstname.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        </FormLabel>
                        <Input
                            type="text"
                            placeholder={t(
                                'register.form.firstname.placeholder'
                            )}
                            isRequired
                            {...register('firstname', {
                                required: t('form.required').toString(),
                            })}
                        />
                        {errors.firstname !== undefined && (
                            <FormErrorMessage>
                                {errors.firstname.message}
                            </FormErrorMessage>
                        )}
                    </FormControl>
                </Box>
                <Box>
                    <FormControl
                        id="lastname"
                        isInvalid={errors.lastname !== undefined}
                    >
                        <FormLabel>
                            <>
                                {t('register.form.lastname.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        </FormLabel>
                        <Input
                            type="text"
                            placeholder={t(
                                'register.form.lastname.placeholder'
                            )}
                            isRequired
                            {...register('lastname', {
                                required: t('form.required').toString(),
                            })}
                        />
                        {errors.lastname !== undefined && (
                            <FormErrorMessage>
                                {errors.lastname.message}
                            </FormErrorMessage>
                        )}
                    </FormControl>
                </Box>
            </SimpleGrid>
            <SimpleGrid columns={{ base: 1, sm: 2 }} spacingX={10} spacingY={3}>
                <Box>
                    <FormControl
                        id="password"
                        isInvalid={errors.password !== undefined}
                    >
                        <FormLabel>
                            <>
                                {t('register.form.password.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        </FormLabel>
                        <InputGroup size="md">
                            <Input
                                type={showPassword ? 'text' : 'password'}
                                isRequired
                                autoComplete="off"
                                {...register('password', {
                                    required: t('form.required').toString(),
                                    minLength: {
                                        value: 4,
                                        message: 'Minimum length should be 4',
                                    },
                                })}
                            />
                            <InputRightElement width="3.5rem">
                                <Button
                                    h="1.75rem"
                                    size="sm"
                                    onClick={() =>
                                        setShowPassword(!showPassword)
                                    }
                                >
                                    {showPassword ? (
                                        <FaRegEyeSlash />
                                    ) : (
                                        <FaRegEye />
                                    )}
                                </Button>
                            </InputRightElement>
                        </InputGroup>
                    </FormControl>
                </Box>
                <Box>
                    <PhoneNumberInput
                        name="phoneNumberObject"
                        countryCodeRegister={register(
                            `phoneNumberObject.countryCode`,
                            {
                                valueAsNumber: true,
                            }
                        )}
                        nationalNumberRegister={register(
                            `phoneNumberObject.nationalNumber`
                        )}
                        isRequired
                        // @ts-ignore
                        errors={errors.phoneNumberObject}
                    />
                </Box>
            </SimpleGrid>
            <SimpleGrid
                columns={{ base: 1, sm: 3 }}
                mt={3}
                spacingX={10}
                spacingY={3}
            >
                <Box>
                    <FormControl
                        id="streetAddress"
                        isInvalid={errors.streetAddress !== undefined}
                    >
                        <FormLabel>
                            <>
                                {t('register.form.streetAddress.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        </FormLabel>
                        <Textarea
                            placeholder={t(
                                'register.form.streetAddress.placeholder'
                            )}
                            isRequired
                            {...register('streetAddress', {
                                required: t('form.required').toString(),
                            })}
                        />
                    </FormControl>
                </Box>
                <Box>
                    <FormControl
                        id="city"
                        isInvalid={errors.city !== undefined}
                    >
                        <FormLabel>
                            <>
                                {t('register.form.city.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        </FormLabel>
                        <Input
                            type="text"
                            placeholder={t('register.form.city.placeholder')}
                            isRequired
                            {...register('city', {
                                required: t('form.required').toString(),
                            })}
                        />
                        {/* errors.city !== undefined && (
                            <FormErrorMessage>
                                {errors.city.message}
                            </FormErrorMessage>
                        ) */}
                    </FormControl>
                </Box>
                <Box>
                    <FormControl
                        id="country"
                        isInvalid={errors.country !== undefined}
                    >
                        <FormLabel>
                            <>
                                {t('register.form.country.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        </FormLabel>
                        <CountryForm
                            register={register('country', {
                                required: t('form.required').toString(),
                            })}
                        />
                    </FormControl>
                </Box>
            </SimpleGrid>
            <Wrap spacing={10} justify="center" mt={10}>
                <PrimaryButton
                    fontFamily="heading"
                    mt={8}
                    isLoading={isSubmitting}
                    type="submit"
                >
                    {t('register.form.button')}
                </PrimaryButton>
            </Wrap>
        </form>
    )
}

export default RegisterForm
