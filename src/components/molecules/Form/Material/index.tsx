import React, { useState } from 'react'
import 'react-phone-number-input/style.css'
import { Stack, useColorModeValue } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { generatePath } from 'react-router-dom'
import { Material } from '../../../../types'
import InformationMaterialForm from './InformationMaterialForm'
import MediaMaterialForm from './MediaMaterialForm'
import PricingMaterialForm from './PricingMaterialForm'
import { H1 } from '../../../atoms/Heading'

const MaterialForm = ({
    material,
    initialStep,
}: {
    material?: Material
    initialStep: 'information' | 'media' | 'pricing'
}) => {
    const [step, setStepState] = useState(initialStep)
    const { t } = useTranslation()
    const setStep = (newStep: 'information' | 'media' | 'pricing') => {
        setStepState(newStep)
        window.history.replaceState(
            null,
            t('pooling.edit.title', { context: newStep, material }),
            generatePath(`/my/materials/:id/edit/:step`, {
                id: material?._id || '',
                step: newStep,
            })
        )
    }

    return (
        <Stack
            bg={useColorModeValue('white', 'gray.600')}
            rounded="xl"
            p={{ base: 4, sm: 6, md: 8 }}
            my={{ base: 4, sm: 6, md: 8 }}
            w="100%"
        >
            <H1>
                {t('pooling.form.title', {
                    context: material ? 'edit' : 'new',
                    material,
                })}
            </H1>
            {(step === 'information' && (
                <InformationMaterialForm
                    setStep={setStep}
                    material={material}
                />
            )) ||
                (step === 'media' && material && (
                    <MediaMaterialForm material={material} setStep={setStep} />
                )) ||
                (step === 'pricing' && (
                    <PricingMaterialForm
                        setStep={setStep}
                        material={material}
                    />
                ))}
        </Stack>
    )
}

export default MaterialForm
