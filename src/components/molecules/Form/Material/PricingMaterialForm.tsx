import { Box, Button, Flex } from '@chakra-ui/react'
import { FieldValues } from 'react-hook-form'
import React from 'react'
import { useTranslation } from 'react-i18next'
import 'react-phone-number-input/style.css'
import { useApolloClient, useMutation, useQuery } from '@apollo/client'
import { FaArrowLeft } from 'react-icons/fa'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import {
    createMaterialPricing,
    deleteMaterialPricing,
    updateMaterialPricing,
} from '../../../../repositories/Material/MaterialRepository'
import MaterialPricingForm from './Pricing/MaterialPricingForm'
import { Material, Pricing, User } from '../../../../types'
import useAuth from '../../../../auth/useAuth'
import { findUserById } from '../../../../repositories/User/UserRepository'
import Loader from '../../../atoms/Loader/Loader'
import Card from '../../Cards/Card'
import { H2, H3 } from '../../../atoms/Heading'
import { Wip } from '../../../atoms/Beta/Beta'

const PricingMaterialForm = ({
    material,
    setStep,
}: {
    material?: Material
    setStep: (newStep: 'information' | 'media' | 'pricing') => void
}) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const client = useApolloClient()
    const auth = useAuth()
    const { data, loading } = useQuery(findUserById, {
        variables: {
            id: auth.user?.id,
        },
    })
    let user: User | null = null
    if (!loading) {
        user = data.user
    }
    const [createPricing] = useMutation(createMaterialPricing, {
        update() {
            client.resetStore()
        },
    })
    const [updatePricing] = useMutation(updateMaterialPricing, {
        update() {
            client.resetStore()
        },
    })
    const [deletePricing] = useMutation(deleteMaterialPricing, {
        update() {
            client.resetStore()
        },
    })

    const onSubmitCreate = (values: FieldValues) => {
        createPricing({
            variables: {
                material: material?.id,
                value: values.price,
                period: 1,
            },
        })
        toast(t('pooling.form.pricing.toast.create.success'))
    }
    const onSubmitUpdate = (values: FieldValues) => {
        updatePricing({
            variables: {
                id: values.id,
                value: values.price,
                period: 1,
                circles: values.circles,
            },
        })
        toast(t('pooling.form.pricing.toast.update.success'))
    }
    const onDelete = (pricing: Pricing) => {
        deletePricing({
            variables: {
                id: pricing.id,
            },
        })
        toast(t('pooling.form.pricing.toast.delete.success'))
    }

    return (
        (user && material && (
            <>
                <H2>
                    <>
                        {t('pooling.form.pricing.title')} <Wip />
                    </>
                </H2>
                {material.pricings.collection.map((pricing) => (
                    <div key={pricing.id}>
                        <Card>
                            <MaterialPricingForm
                                onSubmit={onSubmitUpdate}
                                onDelete={onDelete}
                                pricing={pricing}
                                circles={user?.circles?.collection}
                            />
                        </Card>
                    </div>
                ))}

                <Box pt={10} alignContent="center">
                    <H3>{t('pooling.form.pricing.new.title')}</H3>
                    <Card>
                        <MaterialPricingForm
                            onSubmit={onSubmitCreate}
                            circles={user?.circles?.collection}
                            submitIcon={<>{t('global.add')}</>}
                        />
                    </Card>
                </Box>
                <Flex
                    justifyContent={{
                        base: undefined,
                        sm: 'space-between',
                    }}
                >
                    <Button
                        fontFamily="heading"
                        mt={8}
                        mr={2}
                        _hover={{
                            boxShadow: 'xl',
                        }}
                        onClick={() => setStep('media')}
                        leftIcon={<FaArrowLeft />}
                        title={t('global.prev')}
                    />
                    <Button
                        variant="ghost"
                        fontFamily="heading"
                        mt={8}
                        w={{ base: 'full', md: 'auto' }}
                        _hover={{
                            bgGradient: 'linear(to-r, red.400,pink.400)',
                            boxShadow: 'xl',
                        }}
                        onClick={() => navigate(`/pooling/${material?._id}`)}
                    >
                        {t('global.goToSomething', {
                            something: material?.name,
                        })}
                    </Button>
                </Flex>
            </>
        )) || <Loader />
    )
}

export default PricingMaterialForm
