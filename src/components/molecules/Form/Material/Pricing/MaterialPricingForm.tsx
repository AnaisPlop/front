import {
    Button,
    Checkbox,
    Flex,
    FormLabel,
    NumberDecrementStepper,
    NumberIncrementStepper,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    SimpleGrid,
    Tooltip,
    Wrap,
} from '@chakra-ui/react'
import { FaSave, FaTrashAlt } from 'react-icons/fa'
import React, { ReactElement } from 'react'
import { FieldValues, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Circle, Pricing } from '../../../../../types'

const MaterialPricingForm = ({
    circles,
    pricing,
    onDelete,
    onSubmit,
    submitIcon,
}: {
    pricing?: Pricing
    circles?: [Circle]
    onDelete?: (pricing: Pricing) => void
    onSubmit: (values: FieldValues) => void
    submitIcon?: ReactElement
}) => {
    const { handleSubmit, register } = useForm<FieldValues>()
    const { t } = useTranslation()

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Wrap align="end">
                <FormLabel w={{ base: '100%', md: 'auto' }}>
                    {t('pooling.form.pricing.label', { currency: '€' })}
                    {pricing?.id && (
                        <input
                            type="hidden"
                            {...register('id', { value: pricing.id })}
                        />
                    )}
                    <NumberInput min={0}>
                        <NumberInputField
                            placeholder={t('pooling.form.pricing.placeholder')}
                            required
                            {...register('price', {
                                value: pricing?.value ?? 0,
                                valueAsNumber: true,
                            })}
                        />
                        <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                        </NumberInputStepper>
                    </NumberInput>
                </FormLabel>
                {circles && circles.length > 0 && (
                    <FormLabel>
                        <Tooltip hasArrow label={t('global.meaning.circles')}>
                            {t('pooling.form.pricing.circles')}
                        </Tooltip>
                        <SimpleGrid columns={4} gap={5}>
                            {circles?.map((circle) => {
                                const defaultChecked =
                                    pricing?.circles.collection.map(
                                        (item) => item.id === circle.id
                                    )
                                const checkboxOpt = {
                                    defaultChecked:
                                        defaultChecked?.includes(true),
                                }
                                return (
                                    <Checkbox
                                        key={circle._id}
                                        size="lg"
                                        colorScheme="yellow"
                                        {...register('circles')}
                                        value={circle.id}
                                        {...checkboxOpt}
                                    >
                                        {circle.name}
                                    </Checkbox>
                                )
                            })}
                        </SimpleGrid>
                    </FormLabel>
                )}
            </Wrap>
            <Flex justifyContent="end" gap="2">
                <Button
                    type="submit"
                    fontFamily="heading"
                    mt={8}
                    w={{ base: 'full', md: 'auto' }}
                    bgGradient="linear(to-r, red.400,pink.400)"
                    color="white"
                    _hover={{
                        bgGradient: 'linear(to-r, red.400,pink.400)',
                        boxShadow: 'xl',
                    }}
                    title={t('global.save')}
                >
                    {submitIcon ?? <FaSave />}
                </Button>
                {onDelete && pricing && (
                    <Button
                        fontFamily="heading"
                        mt={8}
                        variant="ghost"
                        w={{ base: 'full', md: 'auto' }}
                        title={t('global.delete')}
                        onClick={() => onDelete(pricing)}
                    >
                        <FaTrashAlt />
                    </Button>
                )}
            </Flex>
        </form>
    )
}

export default MaterialPricingForm
