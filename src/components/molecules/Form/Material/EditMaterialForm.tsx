import React, { Dispatch, SetStateAction, useState } from 'react'
import { Button, Heading, Input, useDisclosure } from '@chakra-ui/react'
import { FieldValues, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Material } from '../../../../types'

const MediaMaterialForm = ({
    material,
    setStep,
}: {
    material: Material
    setStep: Dispatch<SetStateAction<'information' | 'media' | 'pricing'>>
}) => {
    const { i18n } = useTranslation()
    const [images] = useState(material.images.edges)
    const { isOpen, onOpen, onClose } = useDisclosure()
    const { register } = useForm<FieldValues>()
    const submit = (file: File) => {
        const formData = new FormData()
        formData.append('imageFile', file)
        formData.append('material', material._id)
        const requestOptions = {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Accept-Language': i18n.language,
                'Content-Type': 'application/json',
            },
            body: formData,
        }

        fetch(
            `${process.env.REACT_APP_API_URL}/material_images`,
            requestOptions
        )
            .then((response) => response.json())
            .then((response) => {
                if (
                    ['hydra:Error', 'ConstraintViolationList'].includes(
                        response['@type']
                    )
                ) {
                    throw new Error(response['hydra:description'])
                }
            })
    }

    const newImageForm = (
        <Input
            type="file"
            w="full"
            {...register('avatar')}
            onChange={(e) => {
                if (e.target && e.target.files) {
                    submit(e.target.files[0])
                    onClose()
                }
            }}
        />
    )
    return (
        <div>
            <Heading as="h2">Images</Heading>
            {images?.map(({ node }) => {
                return (
                    <div key={node.id}>
                        {node.imageName}
                        <Button>Remove</Button>
                    </div>
                )
            })}
            <Button onClick={onOpen}>Add</Button>
            {isOpen && newImageForm}
            <Button onClick={() => setStep('information')}>Previous</Button>
            <Button onClick={() => setStep('pricing')}>Next</Button>
        </div>
    )
}

export default MediaMaterialForm
