import React, { useState } from 'react'
import {
    Button,
    Flex,
    HStack,
    Image,
    Input,
    Spinner,
    useDisclosure,
} from '@chakra-ui/react'
import { FieldValues, useForm } from 'react-hook-form'
import { useApolloClient } from '@apollo/client'
import { useTranslation } from 'react-i18next'
import { FaArrowLeft, FaArrowRight, FaPlus } from 'react-icons/fa'
import { Material, MaterialImage, MaterialImageEdge } from '../../../../types'
import { H2 } from '../../../atoms/Heading'

type MediaHydraResponse = {
    '@type': string
    'hydra:description': string
} & MaterialImage

const MediaMaterialForm = ({
    material,
    setStep,
}: {
    material: Material
    setStep: (newStep: 'information' | 'media' | 'pricing') => void
}) => {
    const [images, setImages] = useState<MaterialImageEdge[]>(
        material.images.edges
    )
    const { isOpen, onOpen, onClose } = useDisclosure()
    const {
        register,
        formState: { isSubmitting },
    } = useForm<FieldValues>()
    const client = useApolloClient()
    const { t, i18n } = useTranslation()
    const submit = (file: File) => {
        const formData = new FormData()
        formData.append('imageFile', file)
        formData.append('material', material._id)
        const requestOptions = {
            method: 'POST',
            headers: {
                'Accept-Language': i18n.language,
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: formData,
        }

        fetch(
            `${process.env.REACT_APP_API_URL}/material_images`,
            requestOptions
        )
            .then((response) => response.json())
            .then((response: MediaHydraResponse) => {
                if (
                    ['hydra:Error', 'ConstraintViolationList'].includes(
                        response['@type']
                    )
                ) {
                    throw new Error(response['hydra:description'])
                }
                setImages([...images, { node: response }])
                client.resetStore()
            })
    }

    const newImageForm = (
        <Input
            type="file"
            w="full"
            {...register('image')}
            onChange={(e) => {
                if (e.target && e.target.files) {
                    submit(e.target.files[0])
                    onClose()
                }
            }}
        />
    )
    return (
        <>
            <H2>{t('pooling.form.images.title')}</H2>
            <HStack>
                {images?.map(({ node }) => {
                    return (
                        <div key={`${material._id}-image-${node._id}`}>
                            <Image
                                src={`${process.env.REACT_APP_API_URL}/images/materials/${node.imageName}`}
                                maxH={100}
                            />
                        </div>
                    )
                })}
                <Button onClick={onOpen}>
                    <FaPlus />
                </Button>
            </HStack>
            {isOpen && newImageForm}
            <Flex justifyContent={{ base: undefined, sm: 'space-between' }}>
                <Button
                    fontFamily="heading"
                    mt={8}
                    mr={2}
                    _hover={{
                        boxShadow: 'xl',
                    }}
                    onClick={() => setStep('information')}
                    leftIcon={<FaArrowLeft />}
                    title={t('global.prev')}
                />
                <Button
                    fontFamily="heading"
                    mt={8}
                    w={{ base: 'full', md: 'auto' }}
                    bgGradient="linear(to-r, red.400,pink.400)"
                    color="white"
                    _hover={{
                        bgGradient: 'linear(to-r, red.400,pink.400)',
                        boxShadow: 'xl',
                    }}
                    rightIcon={
                        (isSubmitting && <Spinner />) || <FaArrowRight />
                    }
                    onClick={() => setStep('pricing')}
                >
                    {t('global.next')}
                </Button>
            </Flex>
        </>
    )
}

export default MediaMaterialForm
