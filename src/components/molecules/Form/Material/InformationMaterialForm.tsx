import {
    Input,
    Button,
    SimpleGrid,
    FormLabel,
    Select,
    Textarea,
    Spinner,
    Box,
    Flex,
    Wrap,
    ButtonGroup,
} from '@chakra-ui/react'
import { FieldValues, useForm } from 'react-hook-form'
import React from 'react'
import { useTranslation } from 'react-i18next'
import 'react-phone-number-input/style.css'
import { toast } from 'react-toastify'
import { useApolloClient, useMutation, useQuery } from '@apollo/client'
import { generatePath, useNavigate } from 'react-router-dom'
import { FaArrowRight, FaTrashAlt } from 'react-icons/fa'
import getCategories from '../../../../repositories/Material/MaterialCategoryRepository'
import { Material, MaterialCategory } from '../../../../types'
import { H2 } from '../../../atoms/Heading'
import { deleteMaterialById } from '../../../../repositories/Material/MaterialRepository'

const InformationMaterialForm = ({
    material,
    setStep,
}: {
    material?: Material
    setStep: (newStep: 'information' | 'media' | 'pricing') => void
}) => {
    const {
        handleSubmit,
        register,
        formState: { isSubmitting },
    } = useForm<FieldValues>()
    const { t, i18n } = useTranslation()
    const client = useApolloClient()
    const navigate = useNavigate()

    const onSubmit = (values: FieldValues) => {
        const requestOptions = {
            method: material ? 'PUT' : 'POST',
            headers: {
                'Accept-Language': i18n.language,
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(values, null, 2),
        }
        const url = `${process.env.REACT_APP_API_URL}/materials${
            material ? `/${material?._id}` : ''
        }`
        fetch(url, requestOptions)
            .then((response) => response.json())
            .then((response) => {
                if (
                    ['hydra:Error', 'ConstraintViolationList'].includes(
                        response['@type']
                    )
                ) {
                    throw new Error(response['hydra:description'])
                }
                if (!material) {
                    toast(t('pooling.new.toast.success'))
                }
                if (material) {
                    client.resetStore()
                    setStep('media')
                } else {
                    navigate(
                        generatePath('/my/materials/:id/edit/:step', {
                            id: response.id,
                            step: 'media',
                        })
                    )
                }
            })
            .catch((error) => {
                const errorMessage = t(
                    `register.toast.error.${Math.floor(Math.random() * 5)}`,
                    { error: error.toString().replace(/^Error: /, '') }
                )
                toast.error(errorMessage)
            })
    }
    const [deleteMaterial] = useMutation(deleteMaterialById, {
        update() {
            client.resetStore()
        },
    })
    const onDelete = (_material: Material) => {
        deleteMaterial({
            variables: {
                id: _material.id,
            },
        })
        toast(t('pooling.form.toast.delete.success'))
        navigate('/')
    }

    const { loading, error, data } = useQuery(getCategories)
    if (error) return <p>Error :(</p>
    let categories = []
    if (!loading) {
        categories = data.materialCategories.edges
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <H2>{t('pooling.form.informations.title')}</H2>
            <Flex direction={{ base: 'column', md: 'row' }}>
                <Box flexShrink={1}>
                    <FormLabel w={{ base: '100%', md: 'auto' }}>
                        {t('pooling.form.name.label')}
                        <Input
                            type="text"
                            placeholder={t('pooling.form.name.placeholder')}
                            isRequired
                            {...register('name', {
                                value: material?.name,
                                required: t('form.errors.required').toString(),
                                minLength: {
                                    value: 4,
                                    message: t('form.errors.tooShort', {
                                        count: 4,
                                    }),
                                },
                            })}
                        />
                    </FormLabel>
                    <SimpleGrid columns={2}>
                        <FormLabel>
                            {t('pooling.form.category.label')}
                            <Select
                                isRequired={false}
                                disabled={loading}
                                {...register('category', {
                                    required: 'This is required',
                                    value: material?.category?.id,
                                })}
                            >
                                {!loading &&
                                    categories.map(
                                        ({
                                            node,
                                        }: {
                                            node: MaterialCategory
                                        }) => {
                                            return (
                                                <option
                                                    key={node.id}
                                                    value={node.id}
                                                >
                                                    {node.name}
                                                </option>
                                            )
                                        }
                                    )}
                            </Select>
                        </FormLabel>
                        <FormLabel w={{ base: '100%', md: 'auto' }}>
                            {t('pooling.form.brand.label')}
                            <Input
                                type="text"
                                required={false}
                                {...register('brand', {
                                    required: false,
                                    value: material?.brand,
                                })}
                            />
                        </FormLabel>
                        <FormLabel>
                            {t('pooling.form.model.label')}
                            <Input
                                type="text"
                                placeholder={t(
                                    'pooling.form.model.placeholder'
                                )}
                                isRequired={false}
                                {...register('model', {
                                    required: false,
                                    value: material?.model,
                                })}
                            />
                        </FormLabel>
                        <FormLabel w={{ base: '100%', md: 'auto' }}>
                            {t('pooling.form.reference.label')}
                            <Input
                                type="text"
                                placeholder={t(
                                    'pooling.form.reference.placeholder'
                                )}
                                isRequired={false}
                                {...register('reference', {
                                    required: false,
                                    value: material?.reference,
                                })}
                            />
                        </FormLabel>
                    </SimpleGrid>
                </Box>
                <Box flexGrow={1}>
                    <FormLabel w="100%">
                        {t('pooling.form.description.label')}
                        <Textarea
                            rows={10}
                            placeholder={t(
                                'pooling.form.description.placeholder'
                            )}
                            isRequired
                            {...register('description', {
                                required: t('form.errors.required').toString(),
                                minLength: {
                                    value: 4,
                                    message: t('form.errors.tooShort', {
                                        count: 4,
                                    }),
                                },
                                value: material?.description,
                            })}
                        />
                    </FormLabel>
                </Box>
            </Flex>
            <Wrap spacing="30px" justify={{ base: 'center', md: 'right' }}>
                <ButtonGroup spacing="3">
                    {onDelete && material && (
                        <Button
                            fontFamily="heading"
                            mt={8}
                            variant="ghost"
                            w={{ base: 'full', md: 'auto' }}
                            title={t('global.delete')}
                            onClick={() => onDelete(material)}
                        >
                            <FaTrashAlt />
                        </Button>
                    )}
                    <Button
                        fontFamily="heading"
                        mt={8}
                        w={{ base: 'full', md: 'auto' }}
                        bgGradient="linear(to-r, red.400,pink.400)"
                        color="white"
                        disabled={isSubmitting}
                        isLoading={isSubmitting}
                        _hover={{
                            bgGradient: 'linear(to-r, red.400,pink.400)',
                            boxShadow: 'xl',
                        }}
                        rightIcon={
                            (isSubmitting && <Spinner />) || <FaArrowRight />
                        }
                        type="submit"
                    >
                        {t('global.next')}{' '}
                    </Button>
                </ButtonGroup>
            </Wrap>
        </form>
    )
}

export default InformationMaterialForm
