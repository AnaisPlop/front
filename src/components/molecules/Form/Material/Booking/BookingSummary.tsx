import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Text,
    Textarea,
    useColorModeValue,
} from '@chakra-ui/react'
import * as React from 'react'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import { Dispatch, SetStateAction } from 'react'
import moment, { locale } from 'moment'
import 'moment/locale/fr'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import { useApolloClient, useMutation } from '@apollo/client'
import { FieldValues, useForm } from 'react-hook-form'
import { ButtonState, Material, MaterialBooking } from '../../../../../types'
import BookingContext from '../../../../../contexts/BookingContext'
import { applyMaterialBooking } from '../../../../../repositories/Material/MaterialBookingRepository'
import useAuth from '../../../../../auth/useAuth'
import moneyFormat from '../../../../../utils/money'

interface BookingSummaryProps {
    booking: MaterialBooking
    material: Material
    setButtonState: Dispatch<SetStateAction<ButtonState | null>>
    buttonState: ButtonState | null
    setStep: Dispatch<SetStateAction<'initial' | 'choosePeriod' | 'pending'>>
}

const BookingSummary = ({
    booking,
    material,
    buttonState,
    setButtonState,
    setStep,
}: BookingSummaryProps) => {
    const { t, i18n } = useTranslation()
    const client = useApolloClient()
    locale(i18n.language)
    const auth = useAuth()

    const [apply] = useMutation(applyMaterialBooking, {
        update() {
            client.resetStore()
        },
    })
    const bookingContext = React.useContext(BookingContext)
    const { register, handleSubmit } = useForm<FieldValues>()

    const onSubmit = (data: FieldValues) => {
        setButtonState({
            loadingText: t('pooling.show.booking.summary.button.pending'),
            bgGradient: 'linear(to-l, purple.400, pink.400)',
            color: 'white',
            isLoading: true,
        })

        apply({
            variables: {
                id: data.id,
                message: data.message,
            },
        }).then((r) => {
            if (r.data.changeStatusMaterialBooking.materialBooking === null) {
                toast.error(
                    t(
                        `pooling.show.booking.validate.error.${Math.floor(
                            Math.random() * 5
                        )}`
                    )
                )
                return
            }
            setStep('pending')
            bookingContext?.setBooking(
                r.data.changeStatusMaterialBooking.materialBooking
            )
        })
    }

    return (
        <Box color="gray.500" textAlign="center">
            <Text m="1rem" fontSize="lg">
                {t('pooling.show.booking.summary.introduction', {
                    owner: material.owner,
                })}
            </Text>
            <ul>
                {booking?.periods.edges.map(({ node: period }) => {
                    return (
                        <li key={`period-${period.id}`}>
                            {(moment(period.startDate).format('L') !==
                                moment(period.endDate).format('L') &&
                                t(
                                    'pooling.show.booking.summary.periodItem.range.label',
                                    {
                                        startDate: moment(
                                            period.startDate
                                        ).format('LL'),
                                        endDate: moment(period.endDate).format(
                                            'LL'
                                        ),
                                        price: period.price,
                                    }
                                )) ||
                                t(
                                    'pooling.show.booking.summary.periodItem.singleDay.label',
                                    {
                                        day: moment(period.startDate).format(
                                            'LL'
                                        ),
                                    }
                                )}
                        </li>
                    )
                })}
            </ul>
            <Text>
                {t('pooling.show.booking.summary.price', {
                    context:
                        booking.price && booking.price > 0 ? 'cost' : 'free',
                    price: moneyFormat(booking.price ?? 0, i18n.language),
                })}
            </Text>
            <form onSubmit={handleSubmit(onSubmit)}>
                <FormControl id="message" my="5">
                    <FormLabel>
                        {t('pooling.show.booking.summary.message.label')}
                    </FormLabel>
                    <input
                        type="hidden"
                        {...register('id', { value: booking.id })}
                    />
                    <Textarea
                        borderColor="gray.300"
                        _hover={{
                            borderRadius: 'gray.300',
                        }}
                        rows={10}
                        placeholder={t(
                            'pooling.show.booking.summary.message.placeholder',
                            {
                                user: material.owner,
                                me: auth?.user?.firstname,
                            }
                        )}
                        {...register('message', {})}
                    />
                </FormControl>
                <Button
                    bgColor={useColorModeValue('blue.400', 'yellow.400')}
                    color={useColorModeValue('yellow', 'blue.700')}
                    flex={1}
                    fontSize="sm"
                    type="submit"
                    mt="1rem"
                    {...buttonState}
                    boxShadow="0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)"
                    _hover={{
                        bgGradient: 'linear(to-l, purple.400, pink.400)',
                        color: 'yellow',
                        transform: 'translateY(2px)',
                        boxShadow: 'lg',
                    }}
                >
                    {t('pooling.show.booking.summary.button.apply')}
                </Button>
            </form>
        </Box>
    )
}

export default BookingSummary
