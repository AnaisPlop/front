import * as React from 'react'
import {
    Box,
    Button,
    Heading,
    Spacer,
    useColorModeValue,
    VStack,
} from '@chakra-ui/react'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import { DateRangePicker, FocusedInputShape, isSameDay } from 'react-dates'
import { Dispatch, SetStateAction, useEffect, useState } from 'react'
import moment, { locale } from 'moment'
import 'moment/locale/fr'
import { FaArrowLeft } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { useApolloClient, useMutation } from '@apollo/client'
import { toast } from 'react-toastify'
import { ButtonState, Material } from '../../../../../types'
import BookingSummary from './BookingSummary'
import BookingContext from '../../../../../contexts/BookingContext'
import { estimateMaterialBooking } from '../../../../../repositories/Material/MaterialBookingRepository'

const AvailabilityPlanning = ({
    material,
    onBack,
    setStep,
}: {
    material: Material
    onBack: () => void
    setStep: Dispatch<SetStateAction<'initial' | 'choosePeriod' | 'pending'>>
}) => {
    const { t, i18n } = useTranslation()
    const [fromDate, setFromDate] = useState<moment.Moment | null>(null)
    const [toDate, setToDate] = useState<moment.Moment | null>(null)
    const [focus, setFocus] = useState<FocusedInputShape | null>('startDate')
    const [buttonState, setButtonState] = useState<null | ButtonState>(null)
    const [summary, setSummary] = useState<Element>()
    const bookingContext = React.useContext(BookingContext)
    const client = useApolloClient()
    locale(i18n.language)

    const [estimate] = useMutation(estimateMaterialBooking, {
        update() {
            client.resetStore()
        },
    })

    const bookedDays: moment.Moment[] = []
    material.bookingPeriods.map(({ startDate, endDate }) => {
        bookedDays.push(moment(startDate))
        let start = moment(startDate)
        const end = moment(endDate)
        if (start < end) {
            while (start < end) {
                const newDate = start.add(1, 'day')
                start = moment(newDate)
                bookedDays.push(newDate)
            }
        }
        bookedDays.push(moment(endDate))

        return true
    })

    useEffect(() => {
        if (bookingContext?.booking) {
            setSummary(
                // @ts-ignore
                <BookingSummary
                    booking={bookingContext?.booking}
                    material={material}
                    setButtonState={setButtonState}
                    buttonState={buttonState}
                    setStep={setStep}
                />
            )
        }
    }, [bookingContext, buttonState, material, setStep])

    return (
        <Box
            w="full"
            bg={useColorModeValue('white', 'gray.900')}
            boxShadow="2xl"
            rounded="lg"
            p={6}
        >
            {onBack && (
                <Button onClick={onBack} leftIcon={<FaArrowLeft />}>
                    {t('back')}
                </Button>
            )}
            <VStack>
                <>
                    <Heading as="h2" size="lg">
                        {t('pooling.show.booking.choosePeriod.title')}
                    </Heading>
                    <Spacer />
                    <DateRangePicker
                        startDatePlaceholderText={t(
                            'pooling.show.booking.choosePeriod.startDate'
                        )}
                        endDatePlaceholderText={t(
                            'pooling.show.booking.choosePeriod.endDate'
                        )}
                        showClearDates
                        hideKeyboardShortcutsPanel
                        startDate={fromDate} // momentPropTypes.momentObj or null,
                        startDateId="availabilityBookingInput_StartDate" // PropTypes.string.isRequired,
                        endDate={toDate} // momentPropTypes.momentObj or null,
                        endDateId="availabilityBookingInput_EndDate" // PropTypes.string.isRequired,
                        onDatesChange={({ startDate, endDate }) => {
                            setFromDate(startDate)
                            setToDate(endDate)
                            if (startDate && endDate) {
                                estimate({
                                    variables: {
                                        startDate: startDate.format(),
                                        endDate: endDate.format(),
                                        materialId: material.id,
                                    },
                                }).then((r) => {
                                    if (
                                        r.data.estimateMaterialBooking
                                            .materialBooking === null
                                    ) {
                                        toast.error(
                                            t(
                                                'pooling.booking.estimate.toast',
                                                {
                                                    context: 'error',
                                                }
                                            )
                                        )
                                        return
                                    }
                                    bookingContext?.setBooking(
                                        r.data.estimateMaterialBooking
                                            .materialBooking
                                    )
                                })
                            }
                        }} // PropTypes.func.isRequired,
                        focusedInput={focus} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                        onFocusChange={(focusedInput) => setFocus(focusedInput)} // PropTypes.func.isRequired,
                        minimumNights={0}
                        numberOfMonths={1}
                        firstDayOfWeek={1}
                        displayFormat="DD/MM/Y"
                        isDayBlocked={(momentDate) => {
                            return (
                                isSameDay(moment(), momentDate) ||
                                bookedDays.some((bookedDay) =>
                                    isSameDay(bookedDay, momentDate)
                                )
                            )
                        }}
                    />
                    <Spacer />

                    {bookingContext?.booking && summary}
                </>
            </VStack>
        </Box>
    )
}

export default AvailabilityPlanning
