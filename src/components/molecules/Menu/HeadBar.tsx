import React from 'react'
import {
    IconButton,
    Avatar,
    Box,
    Flex,
    HStack,
    VStack,
    useColorModeValue,
    Text,
    FlexProps,
    Menu,
    MenuButton,
    MenuItem,
    MenuList,
    Button,
    Link as CLink,
    Container,
} from '@chakra-ui/react'
import { Link } from 'react-router-dom'
import { FiMenu, FiChevronDown } from 'react-icons/fi'
import { GoDiffAdded } from 'react-icons/go'
import { useTranslation } from 'react-i18next'
import { GiUnicorn } from 'react-icons/gi'
import ColorModeSwitcher from '../../../ColorModeSwitcher'
import Logo from '../../atoms/Logo/Logo'
import { LinkItemProps } from '../../layout/LayoutProps'
import NavItem from '../../atoms/Menu/NavItem'
import useAuth from '../../../auth/useAuth'

interface Props extends FlexProps {
    onOpen: () => void
    links: LinkItemProps[]
}

const HeadBar = ({ onOpen, links, ...rest }: Props) => {
    const { t } = useTranslation()
    const auth = useAuth()

    return (
        <Flex
            px={{ base: 4, md: 4 }}
            height="20"
            alignItems="center"
            bg={useColorModeValue('white', 'gray.900')}
            borderBottomWidth="1px"
            borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
            justifyContent={{ base: 'space-between' }}
            {...rest}
        >
            <Container maxW="container.xl">
                <HStack
                    spacing={{ base: '0', md: '6' }}
                    justifyContent="space-between"
                >
                    <IconButton
                        mr={5}
                        display={{ base: 'flex', md: 'none' }}
                        onClick={onOpen}
                        variant="outline"
                        aria-label="open menu"
                        icon={<FiMenu />}
                    />
                    <CLink to="/" style={{ textDecoration: 'none' }} as={Link}>
                        <Logo height="80px" />
                    </CLink>
                    <Flex grow={1}>
                        {links.map((link) => (
                            <NavItem key={link.name} {...link}>
                                {link.name}
                            </NavItem>
                        ))}
                    </Flex>
                    <Flex alignItems="center">
                        <ColorModeSwitcher
                            justifySelf="flex-end"
                            display={{ base: 'none', sm: 'flex' }}
                        />
                        <Link to={(auth.user && '/pooling/new') || '/register'}>
                            <Button
                                size="sm"
                                mr={4}
                                display={{ base: 'none', sm: 'flex' }}
                                leftIcon={<GiUnicorn />}
                                colorScheme="yellow"
                                flex={1}
                                fontSize="sm"
                                boxShadow="0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)"
                                _hover={{
                                    bgGradient:
                                        'linear(to-l, purple.400, pink.400)',
                                    color: 'yellow',
                                    transform: 'translateY(2px)',
                                    boxShadow: 'lg',
                                }}
                            >
                                <Text>{t('layout.head.cta.label')}</Text>
                            </Button>
                        </Link>
                        <Button
                            display={{ base: 'flex', sm: 'none' }}
                            variant="ghost"
                            colorScheme="red"
                        >
                            <Box>
                                <GoDiffAdded size={20} />
                            </Box>
                        </Button>
                        {(localStorage.getItem('token') && auth.user && (
                            <Flex alignItems="center">
                                <Menu>
                                    <MenuButton
                                        py={2}
                                        transition="all 0.3s"
                                        _focus={{ boxShadow: 'none' }}
                                    >
                                        <HStack>
                                            <Avatar
                                                size="sm"
                                                src={`${process.env.REACT_APP_API_URL}${auth.user.avatar?.contentUrl}`}
                                            />
                                            <VStack
                                                display={{
                                                    base: 'none',
                                                    md: 'flex',
                                                }}
                                                alignItems="flex-start"
                                                spacing="1px"
                                                ml="2"
                                            >
                                                <Text fontSize="sm">
                                                    {`${auth.user.firstname} ${auth.user.lastname}`}
                                                </Text>
                                                {auth.user.roles.includes(
                                                    'ROLE_MODERATOR'
                                                ) && (
                                                    <Text
                                                        fontSize="xs"
                                                        color="gray.600"
                                                    >
                                                        Admin
                                                    </Text>
                                                )}
                                            </VStack>
                                            <Box
                                                display={{
                                                    base: 'none',
                                                    md: 'flex',
                                                }}
                                            >
                                                <FiChevronDown />
                                            </Box>
                                        </HStack>
                                    </MenuButton>
                                    <MenuList
                                        // bg={useColorModeValue('white', 'gray.900')}
                                        // borderColor={useColorModeValue('gray.200', 'gray.700')}
                                        zIndex={2}
                                    >
                                        <MenuItem>
                                            <Link to="/my/bookings">
                                                {t('menu.my.bookings')}
                                            </Link>
                                        </MenuItem>
                                        <MenuItem>
                                            <Link to="/logout">
                                                {t('menu.logout')}
                                            </Link>
                                        </MenuItem>
                                    </MenuList>
                                </Menu>
                            </Flex>
                        )) || <Link to="/login">{t('menu.login')}</Link>}
                    </Flex>
                </HStack>
            </Container>
        </Flex>
    )
}

export default HeadBar
