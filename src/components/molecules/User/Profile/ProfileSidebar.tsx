import React, { ReactNode } from 'react'
import {
    Box,
    Flex,
    Icon,
    useColorModeValue,
    Link,
    BoxProps,
    FlexProps,
    FormControl,
} from '@chakra-ui/react'
import { FiUser, FiBell } from 'react-icons/fi'
import { IconType } from 'react-icons'
import { BsCalendar2Week } from 'react-icons/bs'
import { FaHammer } from 'react-icons/fa'
import { useLocation, useNavigate } from 'react-router-dom'
import { Select } from 'chakra-react-select'
import { useTranslation } from 'react-i18next'

interface LinkItemProps {
    name: string
    icon: IconType
    href: string
    disabled?: boolean
}
const LinkItems: Array<LinkItemProps> = [
    { name: 'Profile', icon: FiUser, href: '/my/profile', disabled: true },
    {
        name: 'My materials',
        icon: FaHammer,
        href: '/my/materials',
        disabled: true,
    },
    { name: 'Bookings', icon: BsCalendar2Week, href: '/my/bookings' },
    {
        name: 'Notifications',
        icon: FiBell,
        href: '/my/notifications',
        disabled: true,
    },
    {
        name: 'Circles',
        icon: FiBell,
        href: '/my/circles',
        disabled: true,
    },
]

interface NavItemProps extends FlexProps {
    icon: IconType
    children: ReactNode
    href: string
    disabled?: boolean
}
const NavItem = ({ icon, href, disabled, children, ...rest }: NavItemProps) => {
    const location = useLocation()
    const activeUrl = location.pathname === href
    const activeStyle = disabled
        ? {
              color: 'gray.700',
          }
        : {
              bg: 'yellow.300',
              color: 'orange.700',
          }
    const currentStyle = activeUrl ? activeStyle : {}
    const disabledStyle = disabled ? { color: 'gray' } : {}

    return (
        <Link
            href={activeUrl ? href : '#'}
            style={{ textDecoration: 'none' }}
            _focus={{ boxShadow: 'none' }}
        >
            <Flex
                align="center"
                p="4"
                mx="4"
                borderRadius="lg"
                role="group"
                cursor={disabled ? 'default' : 'pointer'}
                _hover={activeStyle}
                {...currentStyle}
                {...disabledStyle}
                {...rest}
            >
                {icon && <Icon mr="4" fontSize="16" as={icon} />}
                {children}
            </Flex>
        </Link>
    )
}

const SidebarContent = ({ ...rest }: BoxProps) => {
    return (
        <Box
            bg={useColorModeValue('white', 'gray.900')}
            borderRight="1px"
            borderRightColor={useColorModeValue('gray.200', 'gray.700')}
            w={{ base: 'full', md: 60 }}
            pos="absolute"
            py={50}
            {...rest}
        >
            {LinkItems.map((link) => (
                <NavItem key={link.name} {...link}>
                    {link.name}
                </NavItem>
            ))}
        </Box>
    )
}

const MobileNav = ({ ...rest }: FlexProps) => {
    const navigate = useNavigate()
    const { t } = useTranslation()

    return (
        <Flex
            ml={{ base: 0, md: 60 }}
            px={{ base: 4, md: 24 }}
            height="20"
            alignItems="center"
            bg={useColorModeValue('white', 'gray.900')}
            borderBottomWidth="1px"
            borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
            justifyContent="flex-start"
            {...rest}
        >
            <FormControl p={4}>
                <Select
                    isOptionDisabled={(option) => option.disabled ?? false}
                    options={LinkItems.map(({ name, href, disabled }) => {
                        return {
                            value: href,
                            label: name,
                            disabled,
                        }
                    })}
                    placeholder={t('pooling.booking.index.title')}
                    onChange={(item) => {
                        if (
                            item &&
                            item.value !== null &&
                            item.value !== undefined
                        ) {
                            navigate(item.value)
                        }
                    }}
                    size="lg"
                    colorScheme="yellow"
                />
            </FormControl>
        </Flex>
    )
}

const ProfileSidebar = ({ children }: { children: ReactNode }) => {
    return (
        <Box bg={useColorModeValue('gray.100', 'gray.900')}>
            <SidebarContent display={{ base: 'none', md: 'block' }} />
            <MobileNav display={{ base: 'flex', md: 'none' }} />
            <Box ml={{ base: 0, md: 60 }} p="4">
                {children}
            </Box>
        </Box>
    )
}

export default ProfileSidebar
