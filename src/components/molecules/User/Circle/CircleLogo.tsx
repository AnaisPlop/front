import * as React from 'react'
import { Avatar, useColorModeValue } from '@chakra-ui/react'
import { Circle } from '../../../../types'
import CommunityIcon from '../../../atoms/Logo/CommunityIcon'

const CircleLogo = ({ circle }: { circle: Circle }) => {
    return (
        <Avatar
            src={`${process.env.REACT_APP_API_URL}/images/circles/${circle.logoName}`}
            icon={<CommunityIcon width={30} height={30} />}
            border={`1px solid ${useColorModeValue('#eee', '#444')}`}
            boxShadow="lg"
            bg={useColorModeValue('white', 'gray.700')}
            title={circle.name}
        />
    )
}

export default CircleLogo
