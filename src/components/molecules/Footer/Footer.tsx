import {
    Box,
    chakra,
    Container,
    Link as CLink,
    Stack,
    VisuallyHidden,
    useColorModeValue,
    Flex,
    Center,
} from '@chakra-ui/react'
import React, { MouseEventHandler, ReactNode } from 'react'
import {
    FaFacebook,
    FaGitlab,
    FaInstagram,
    FaSymfony,
    FaTwitter,
    FaYoutube,
} from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { SiChakraui, SiGraphql, SiPostgresql } from 'react-icons/si'
import { GiHangingSpider } from 'react-icons/gi'
import { GrGithub, GrReactjs } from 'react-icons/gr'
import Logo from '../../atoms/Logo/Logo'

const SocialButton = ({
    children,
    href,
    label,
    onClick,
    title,
}: {
    children: ReactNode
    href?: string
    label: string
    onClick?: MouseEventHandler<HTMLButtonElement> &
        MouseEventHandler<HTMLAnchorElement>
    title?: string
}) => {
    return (
        <chakra.button
            bg={useColorModeValue('blackAlpha.100', 'whiteAlpha.100')}
            rounded="full"
            w={8}
            h={8}
            cursor="pointer"
            as="a"
            href={href}
            onClick={onClick}
            display="inline-flex"
            alignItems="center"
            title={title}
            justifyContent="center"
            transition="background 0.3s ease"
            _hover={{
                bg: useColorModeValue('blackAlpha.200', 'whiteAlpha.200'),
            }}
        >
            <VisuallyHidden>{label}</VisuallyHidden>
            {children}
        </chakra.button>
    )
}

const Footer = () => {
    const { t, i18n } = useTranslation()
    return (
        <Box
            bg={useColorModeValue('gray.50', 'gray.900')}
            color={useColorModeValue('gray.700', 'gray.200')}
        >
            <Container as={Stack} maxW="6xl" py={10}>
                <Flex justifyContent="space-between">
                    <Stack spacing={6}>
                        <Box>
                            <Logo height={100} width={220} />
                        </Box>
                        <Box
                            w={['auto', 'auto', 'auto']}
                            my="5"
                            fontStyle="italic"
                        >
                            <CLink isExternal href="https://gitlab.com/communo">
                                <GrGithub
                                    style={{
                                        display: 'inline',
                                        marginRight: 2,
                                    }}
                                />
                                {t('layout.footer.license')}
                            </CLink>{' '}
                            {t('layout.footer.librariesCredit.1')}
                            <CLink isExternal href="https://fr.reactjs.org">
                                <GrReactjs
                                    style={{
                                        display: 'inline',
                                        marginRight: 2,
                                    }}
                                />
                                React
                            </CLink>
                            {', '}
                            <CLink isExternal href="https://chakra-ui.com">
                                <SiChakraui
                                    style={{
                                        display: 'inline',
                                        marginRight: 2,
                                    }}
                                />
                                CharkaUI
                            </CLink>
                            {', '}
                            <CLink isExternal href="https://symfony.com">
                                <FaSymfony
                                    style={{
                                        display: 'inline',
                                        marginRight: 2,
                                    }}
                                />
                                Symfony
                            </CLink>
                            {', '}
                            <CLink isExternal href="https://api-platform.com">
                                <GiHangingSpider
                                    style={{
                                        display: 'inline',
                                        marginRight: 2,
                                    }}
                                />
                                Api-Plaform
                            </CLink>
                            {', '}
                            <CLink isExternal href="https://graphql.org">
                                <SiGraphql
                                    style={{
                                        display: 'inline',
                                        marginRight: 2,
                                    }}
                                />
                                GraphQL
                            </CLink>
                            {', '}
                            <CLink isExternal href="https://www.postgresql.org">
                                <SiPostgresql
                                    style={{
                                        display: 'inline',
                                        marginRight: 2,
                                    }}
                                />
                                Postgres
                            </CLink>{' '}
                            {t('layout.footer.librariesCredit.2')}
                        </Box>
                        <Center>
                            <Stack direction="row" spacing={6}>
                                <SocialButton
                                    label={
                                        (i18n.language === 'fr' && 'en') || 'fr'
                                    }
                                    onClick={() =>
                                        i18n.changeLanguage(
                                            i18n.language === 'fr' ? 'en' : 'fr'
                                        )
                                    }
                                    title={t('switch.language')}
                                >
                                    {(i18n.language === 'fr' && 'en') || 'fr'}
                                </SocialButton>
                                <SocialButton
                                    label="Gitlab"
                                    href="https://gitlab.com/communo"
                                >
                                    <FaGitlab />
                                </SocialButton>
                                <SocialButton
                                    label="Facebook"
                                    href="https://facebook.com/letscommuno"
                                >
                                    <FaFacebook />
                                </SocialButton>
                                <SocialButton
                                    label="Twitter"
                                    href="https://twitter.com/letscommuno"
                                >
                                    <FaTwitter />
                                </SocialButton>
                                <SocialButton
                                    label="YouTube"
                                    href="https://www.youtube.com/channel/UC4UQVJCt6xg9GrctD2Guiew"
                                >
                                    <FaYoutube />
                                </SocialButton>
                                <SocialButton
                                    label="Instagram"
                                    href="https://www.instagram.com/letscommuno/"
                                >
                                    <FaInstagram />
                                </SocialButton>
                            </Stack>
                        </Center>
                    </Stack>
                    {/* <Stack align={'flex-start'}>
                        <ListHeader>
                            {t('layout.footer.newsletter.title')}
                        </ListHeader>
                        <Stack direction={'row'}>
                            <Input
                                placeholder={t(
                                    'layout.footer.newsletter.placeholder'
                                )}
                                bg={useColorModeValue(
                                    'blackAlpha.100',
                                    'whiteAlpha.100'
                                )}
                                border={0}
                                _focus={{
                                    bg: 'whiteAlpha.300',
                                }}
                            />
                            <IconButton
                                bg={useColorModeValue('green.400', 'green.800')}
                                color={useColorModeValue('white', 'gray.800')}
                                _hover={{
                                    bg: 'green.600',
                                }}
                                title={t('layout.footer.newsletter.subscribe')}
                                aria-label={t(
                                    'layout.footer.newsletter.subscribe'
                                )}
                                icon={<BiMailSend />}
                            />
                        </Stack>
                    </Stack> */}
                </Flex>
            </Container>
        </Box>
    )
}

export default Footer
