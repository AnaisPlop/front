import React, { ReactElement } from 'react'
import {
    Avatar,
    BackgroundProps,
    Badge,
    Box,
    ColorProps,
    Flex,
    Text,
    Wrap,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import TimeAgo from 'timeago-react'
import moment from 'moment'
import { useApolloClient, useMutation } from '@apollo/client'
import { FaTrashAlt } from 'react-icons/fa'
import { toast } from 'react-toastify'
import { Message } from '../../../types'
import useAuth from '../../../auth/useAuth'
import transformBackendUri from '../../../utils/transformBackendUri'
import { deleteMessage } from '../../../repositories/Discussion/MessageRepository'

type Props = {
    action?: ReactElement
} & Message &
    BackgroundProps &
    ColorProps

const MessageText = ({
    content,
    createdAt,
    action,
    bg = 'gray.100',
    color = 'black',
}: Props) => {
    const { i18n } = useTranslation()

    return (
        <Flex alignItems="center" gap={2}>
            <Flex
                bg={bg}
                borderRadius="xl"
                color={color}
                minW="100px"
                maxW="350px"
                my="1"
                p="3"
                direction="column"
            >
                <Wrap>
                    <Text
                        color="gray.400"
                        textAlign="right"
                        title={moment(createdAt).format('LLLL')}
                    >
                        <TimeAgo datetime={createdAt} locale={i18n.language} />
                    </Text>
                </Wrap>
                <Text>{content}</Text>
            </Flex>
            {action}
        </Flex>
    )
}

const Messages = ({ messages }: { messages: Message[] }) => {
    const auth = useAuth()
    const { t } = useTranslation()
    const client = useApolloClient()

    const [deleteMsg] = useMutation(deleteMessage, {
        update() {
            client.resetStore()
        },
    })
    const onDelete = (message: Message) => {
        deleteMsg({
            variables: {
                id: message.id,
            },
        })
        toast.success('message.delete.success')
    }
    return (
        <Flex w="100%" h="80%" overflowY="scroll" flexDirection="column" p="3">
            {messages.map((item) => {
                if (!item.author) {
                    return (
                        <Box
                            key={item.id}
                            my="1"
                            borderRadius="xl"
                            bg="blue.100"
                            p="3"
                        >
                            <Badge colorScheme="blue" mb="5">
                                {t('discussion.message.automatic')}
                            </Badge>
                            <Text colorScheme="blue">{item.content}</Text>
                        </Box>
                    )
                }
                if (item.author._id === auth.user?._id) {
                    return (
                        <Flex key={item.id} w="100%" justify="flex-end">
                            <MessageText
                                {...item}
                                bg="black"
                                color="white"
                                action={
                                    <FaTrashAlt
                                        onClick={() => onDelete(item)}
                                    />
                                }
                            />
                        </Flex>
                    )
                }
                return (
                    <Flex key={item.id} w="100%">
                        <Avatar
                            name={item.author.fullname}
                            src={transformBackendUri(
                                item.author.avatar?.contentUrl
                            )}
                            bg="blue.300"
                        />
                        <MessageText {...item} />
                    </Flex>
                )
            })}
        </Flex>
    )
}

export default Messages
