import {
    Center,
    useColorModeValue,
    Heading,
    Text,
    Stack,
    Image,
    Button,
    Flex,
    Avatar,
} from '@chakra-ui/react'
import { FaChevronRight } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Material } from '../../../types'
import transformBackendUri from '../../../utils/transformBackendUri'
import Blaze from '../../atoms/Heading/Blaze'

type MaterialHorizontalCardProps = {
    material: Material
}

const MaterialHorizontalCard = ({ material }: MaterialHorizontalCardProps) => {
    const { t } = useTranslation()
    const { images, _id, name, brand, model, owner } = material
    const picture =
        images.edges.length > 0
            ? transformBackendUri(
                  `/images/materials/${images.edges[0].node.imageName}`
              )
            : 'https://images.unsplash.com/photo-1518051870910-a46e30d9db16?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80'

    return (
        <Center>
            <Stack
                borderWidth="1px"
                borderRadius="lg"
                w={{ sm: '100%', md: '540px' }}
                height={{ sm: '476px', md: '20rem' }}
                direction={{ base: 'column', md: 'row' }}
                bg={useColorModeValue('white', 'gray.900')}
                boxShadow="2xl"
                padding={4}
            >
                <Flex flex={1} bg="blue.200">
                    <Image objectFit="cover" boxSize="100%" src={picture} />
                </Flex>
                <Stack
                    flex={1}
                    flexDirection="column"
                    justifyContent="center"
                    alignItems="center"
                    p={1}
                    pt={2}
                >
                    <Heading fontSize="2xl" fontFamily="body">
                        {name}
                    </Heading>
                    <Text fontWeight={600} color="gray.500" size="sm" mb={4}>
                        {brand} - {model}
                    </Text>
                    <Flex alignItems="center" gap={3}>
                        <Avatar
                            src={transformBackendUri(owner.avatar?.contentUrl)}
                        />
                        <Blaze {...owner} />
                    </Flex>
                    <Text>
                        {owner.streetAddress} {owner.city}
                    </Text>

                    <Stack direction="row" align="center">
                        <Link to={`/materials/${_id}`}>
                            <Button
                                rounded="full"
                                px={6}
                                w="full"
                                mt={8}
                                colorScheme={useColorModeValue('red', 'yellow')}
                                textTransform="uppercase"
                                rightIcon={<FaChevronRight />}
                            >
                                {t('global.see')}
                            </Button>
                        </Link>
                    </Stack>
                </Stack>
            </Stack>
        </Center>
    )
}
export default MaterialHorizontalCard
