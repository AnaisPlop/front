import {
    Box,
    Center,
    useColorModeValue,
    Heading,
    Text,
    Stack,
    Image,
    Button,
} from '@chakra-ui/react'
import { FaChevronRight } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Material } from '../../../types'

type ProductCardProps = {
    id: string
    picture: string
    name: string
    model: string
    brand: string
}

const ProductCard = ({ picture, name, model, brand, id }: ProductCardProps) => {
    const { t } = useTranslation()

    return (
        <Center py={12} key={`materialCard${id}`}>
            <Link to={`/pooling/${id}`}>
                <Box
                    role="group"
                    p={6}
                    maxW="330px"
                    w="full"
                    bg={useColorModeValue('white.100', 'gray.700')}
                    boxShadow="2xl"
                    rounded="lg"
                    pos="relative"
                    zIndex={1}
                    _hover={{
                        transform: 'translateY(2px)',
                        boxShadow: 'lg',
                    }}
                >
                    <Box
                        rounded="lg"
                        mt={-12}
                        pos="relative"
                        height="230px"
                        bg="white"
                    >
                        <Image
                            rounded="lg"
                            height={230}
                            width={282}
                            objectFit="cover"
                            src={picture}
                        />
                    </Box>
                    <Stack pt={10} align="center">
                        <Text
                            color="gray.500"
                            fontSize="sm"
                            textTransform="uppercase"
                        >
                            {brand}
                        </Text>
                        <Heading
                            fontSize="2xl"
                            fontFamily="body"
                            fontWeight={500}
                        >
                            {name} {model}
                        </Heading>
                        <Stack direction="row" align="center">
                            <Button
                                rounded="full"
                                px={6}
                                w="full"
                                mt={8}
                                colorScheme={useColorModeValue('red', 'yellow')}
                                textTransform="uppercase"
                                rightIcon={<FaChevronRight />}
                            >
                                {t('global.see')}
                            </Button>
                        </Stack>
                    </Stack>
                </Box>
            </Link>
        </Center>
    )
}
type MaterialCardProps = {
    material: Material
}

const MaterialCard = ({ material }: MaterialCardProps) => {
    const { images, _id, name, brand, model } = material
    const picture =
        images.edges.length > 0
            ? `${process.env.REACT_APP_API_URL}/images/materials/${images.edges[0].node.imageName}`
            : 'https://images.unsplash.com/photo-1518051870910-a46e30d9db16?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80'
    return (
        <ProductCard
            key={_id}
            picture={picture}
            name={name}
            brand={brand}
            model={model}
            id={_id}
        />
    )
}
export default MaterialCard
