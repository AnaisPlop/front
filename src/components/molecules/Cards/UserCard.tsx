import * as React from 'react'
import {
    Avatar,
    Text,
    Stack,
    Button,
    Badge,
    useColorModeValue,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    Spacer,
    useDisclosure,
    Wrap,
    Box,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { HiOutlineChatAlt } from 'react-icons/hi'
import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import { User } from '../../../types'
import ExchangeMailCloud from '../../atoms/Illustrations/ExchangeMailCloud'
import Card from './Card'
import CircleLogo from '../User/Circle/CircleLogo'
import useAuth from '../../../auth/useAuth'
import Blaze from '../../atoms/Heading/Blaze'

const UserCard = ({
    user,
    borderRadius,
}: {
    user: User
    borderRadius?: string
}) => {
    const { t } = useTranslation()
    const {
        isOpen: isContactOpen,
        onOpen: onContactOpen,
        onClose: onContactClose,
    } = useDisclosure()
    const auth = useAuth()
    return (
        <Card borderRadius={borderRadius}>
            <>
                <Avatar
                    size="xl"
                    src={`${process.env.REACT_APP_API_URL}${user.avatar?.contentUrl}`}
                    name={`${user.firstname} ${user.lastname}`}
                    mb={4}
                    pos="relative"
                />
                <Blaze {...user} />
                <Stack direction="column">
                    <Text fontWeight={600} color="gray.500" mb={4}>
                        {user.city}
                    </Text>
                    <Wrap spacing="1rem" justify="center">
                        {user.circles.collection.map((circle) => {
                            return (
                                <CircleLogo key={circle._id} circle={circle} />
                            )
                        })}
                    </Wrap>
                    {/* <Rating */}
                    {/*    readonly */}
                    {/*    initialRating={user.averageRatingScore ?? 0} */}
                    {/* /> */}
                </Stack>

                <Stack align="center" justify="center" direction="row" mt={6}>
                    <Badge
                        px={2}
                        py={1}
                        bg={useColorModeValue('gray.50', 'gray.800')}
                        fontWeight="400"
                    >
                        {t('user.card.ratingsNumber', {
                            count: user.ratings.totalCount,
                        })}
                    </Badge>
                    <Badge
                        px={2}
                        py={1}
                        bg={useColorModeValue('gray.50', 'gray.800')}
                        fontWeight="400"
                    >
                        {t('user.card.sharedItemsNumber', {
                            count: user.materials.paginationInfo.totalCount,
                        })}
                    </Badge>
                </Stack>

                <Stack mt={8} direction="row" spacing={4}>
                    <Button
                        flex={1}
                        fontSize="sm"
                        rounded="full"
                        onClick={onContactOpen}
                        leftIcon={<HiOutlineChatAlt />}
                        disabled={user._id === auth.user?._id}
                        _focus={{
                            bg: 'gray.200',
                        }}
                    >
                        {t('user.card.contact')}
                    </Button>
                    <Modal
                        isOpen={isContactOpen}
                        onClose={onContactClose}
                        isCentered
                    >
                        <ModalOverlay />
                        <ModalContent>
                            <ModalHeader>
                                {t('pooling.show.modal.title', { user })}
                            </ModalHeader>
                            <ModalCloseButton />
                            <ModalBody>
                                <Box whiteSpace="pre-line">
                                    <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                                        {t('pooling.show.modal.body', {
                                            user,
                                        })}
                                    </ReactMarkdown>
                                </Box>
                                <ExchangeMailCloud
                                    height={{ sm: '16rem', lg: '24rem' }}
                                    mt={{ base: 12, sm: 16 }}
                                />
                                <Spacer h={35} />
                            </ModalBody>
                        </ModalContent>
                    </Modal>
                </Stack>
            </>
        </Card>
    )
}

export default UserCard
