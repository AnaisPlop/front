import { Box, Heading, SimpleGrid, Text } from '@chakra-ui/react'
import * as React from 'react'
import { ReactNode } from 'react'
import StatCard from './StatCard'
import EmphasisText from '../../../atoms/Text/EmphasisText'

type Props = {
    heading: string
    subHeading?: string
    stats: {
        description: string
        icon?: ReactNode
        source: {
            name: string
            link: string
        }
        title: string
        value: string
    }[]
}

const StatsWithCard = ({ heading, subHeading, stats }: Props) => {
    return (
        <Box as="section" pb="20">
            <Box bg="blue.600" pt="20" pb="20">
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <Box mb="130" color="white" maxW="xl">
                        <Heading
                            size="2xl"
                            letterSpacing="tight"
                            fontWeight="extrabold"
                            lineHeight="normal"
                        >
                            <EmphasisText color="yellow.400">
                                {heading}
                            </EmphasisText>
                        </Heading>
                        {subHeading && (
                            <Text fontSize="lg" mt="4" fontWeight="medium">
                                {subHeading}
                            </Text>
                        )}
                    </Box>
                </Box>
            </Box>
            <Box mt="-20">
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <SimpleGrid columns={{ base: 1, md: 2, lg: 4 }} spacing="6">
                        {stats &&
                            stats.map(({ title, value, description, icon }) => (
                                <StatCard
                                    key={title}
                                    title={title}
                                    value={value}
                                    icon={icon}
                                >
                                    <EmphasisText color="yellow.400">
                                        {description}
                                    </EmphasisText>
                                </StatCard>
                            ))}
                    </SimpleGrid>
                </Box>
            </Box>
        </Box>
    )
}

export default StatsWithCard
