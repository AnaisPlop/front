import { Box, BoxProps, Text, useColorModeValue } from '@chakra-ui/react'
import * as React from 'react'
import { ReactNode } from 'react'
import EmphasisText from '../../../atoms/Text/EmphasisText'

interface StatCardProps extends BoxProps {
    title: string
    value: string
    icon?: ReactNode
    children: ReactNode
}
const StatCard = (props: StatCardProps) => {
    const { title, value, icon, children } = props
    return (
        <Box
            as="dl"
            bg={useColorModeValue('white', 'gray.700')}
            p="6"
            rounded="lg"
            shadow="md"
        >
            {icon && (
                <Box pos="absolute" m="-90px 90px;">
                    {icon}
                </Box>
            )}
            <Text
                as="dt"
                color={useColorModeValue('blue.500', 'blue.300')}
                fontSize="sm"
                fontWeight="bold"
            >
                {title}
            </Text>
            <Text
                as="dd"
                fontSize="5xl"
                fontWeight="extrabold"
                lineHeight="1"
                my="4"
            >
                <EmphasisText color="yellow.400">{value}</EmphasisText>
            </Text>
            <Text as="dd" color={useColorModeValue('gray.600', 'white')}>
                {children}
            </Text>
        </Box>
    )
}
export default StatCard
