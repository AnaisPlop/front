import * as React from 'react'
import useEvent from 'react-use/lib/useEvent'
import { Controlled as ControlledZoom } from 'react-medium-image-zoom'
import { useCallback, useState } from 'react'
import { Box } from '@chakra-ui/react'
import { Material } from '../../../types'

const MaterialPictures = ({ material }: { material: Material }) => {
    const [activeIndex, setActiveIndex] = useState<number | null>(null)
    const images = material.images.edges.map(({ node }) => node)

    const handleKeyDown = useCallback(
        // @ts-ignoreœ
        (e) => {
            if (activeIndex === null) {
                return
            }

            if (e.key === 'ArrowLeft' || e.keyCode === 37) {
                setActiveIndex(Math.max(activeIndex - 1, 0))
            } else if (e.key === 'ArrowRight' || e.keyCode === 39) {
                setActiveIndex(Math.min(activeIndex + 1, images.length - 1))
            }
        },
        [activeIndex, images.length]
    )

    useEvent('keydown', handleKeyDown, document)

    return (
        <Box w="100%" borderRadius="10px">
            <ul
                style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    listStyle: 'none',
                    margin: 0,
                    padding: 0,
                }}
            >
                {images.map((img, i) => {
                    // eslint-disable-next-line react-hooks/rules-of-hooks
                    const handleZoomChange = useCallback(
                        (isZoomed: boolean) => {
                            if (isZoomed) {
                                setActiveIndex(i)
                                return
                            }
                            setActiveIndex(null)
                        },
                        [i]
                    )

                    return (
                        <li
                            key={img.imageName}
                            style={{
                                margin:
                                    i === 0 ? '0 1rem 0 0' : '0 1rem 1rem 0',
                                width: i === 0 ? '100%' : 'calc(20% - 1rem)',
                            }}
                        >
                            <ControlledZoom
                                isZoomed={activeIndex === i}
                                onZoomChange={handleZoomChange}
                                transitionDuration={0}
                                wrapStyle={{ width: '100%' }}
                            >
                                <div
                                    aria-label="img.alt"
                                    role="img"
                                    style={{
                                        backgroundColor: '#c2c2c2',
                                        backgroundImage: `url("${process.env.REACT_APP_API_URL}/images/materials/${img.imageName}")`,
                                        backgroundPosition: 'center center',
                                        backgroundRepeat: 'no-repeat',
                                        backgroundSize: 'cover',
                                        height: '0',
                                        paddingBottom: '66%',
                                        width: 'calc(100%)',
                                        borderRadius: '5px',
                                    }}
                                />
                            </ControlledZoom>
                        </li>
                    )
                })}
            </ul>
        </Box>
    )
}

export default MaterialPictures
