import { Button } from '@chakra-ui/react'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Material, MaterialLegacy } from '../../../types'

const BookButton = ({
    material,
    onClick,
}: {
    material: Material | MaterialLegacy
    onClick: () => void
}) => {
    const { t } = useTranslation()
    return (
        <Button
            flex={1}
            width="full"
            fontSize="sm"
            bg="green.600"
            color="white"
            onClick={onClick}
            boxShadow="0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)"
            _focus={{
                bg: 'green.300',
            }}
            _hover={{
                bg: 'green.300',
                transform: 'translateY(2px)',
                boxShadow: 'lg',
            }}
        >
            {t('pooling.show.button.label', {
                user: material.owner,
            })}
        </Button>
    )
}

export default BookButton
