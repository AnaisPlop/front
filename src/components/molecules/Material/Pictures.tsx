import * as React from 'react'
import useEvent from 'react-use/lib/useEvent'
import { useCallback, useState } from 'react'
import {
    Box,
    Button,
    Center,
    HStack,
    Image,
    Kbd,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalOverlay,
    useDisclosure,
    VStack,
} from '@chakra-ui/react'
import { FaExpand } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { Material, MaterialLegacy } from '../../../types'

const MaterialPictures = ({
    material,
}: {
    material: Material | MaterialLegacy
}) => {
    const [activeIndex, setActiveIndex] = useState<number>(0)
    const images = material.images.edges.map(({ node }) => node)
    const { t } = useTranslation()

    const handleKeyDown = useCallback(
        // @ts-ignoreœ
        (e) => {
            if (activeIndex === null) {
                return
            }

            if (e.key === 'ArrowLeft' || e.keyCode === 37) {
                setActiveIndex(Math.max(activeIndex - 1, 0))
            } else if (e.key === 'ArrowRight' || e.keyCode === 39) {
                setActiveIndex(Math.min(activeIndex + 1, images.length - 1))
            }
        },
        [activeIndex, images.length]
    )

    useEvent('keydown', handleKeyDown, document)
    const { isOpen, onOpen, onClose } = useDisclosure()

    return (
        <Box
            w="100%"
            borderRadius="10px"
            bgGradient="linear(to-r, gray.200, gray.300)"
        >
            {images.length > 0 && (
                <>
                    <Box position="relative">
                        <Image
                            title={
                                images.length > 1
                                    ? t('pooling.show.pictures.seeOthers')
                                    : ''
                            }
                            cursor="pointer"
                            maxH={455}
                            objectFit="cover"
                            boxShadow="lg"
                            borderRadius={5}
                            w={{ base: '100%' }}
                            src={`${process.env.REACT_APP_API_URL}/images/materials/${images[0].imageName}`}
                            alt={material.name}
                            onClick={onOpen}
                        />
                        <HStack position="absolute" right={2} bottom={2}>
                            <Button
                                size="xs"
                                px={2}
                                mt={8}
                                colorScheme="yellow"
                                onClick={onOpen}
                                variant="solid"
                            >
                                <FaExpand />
                            </Button>
                        </HStack>
                    </Box>
                    <Modal onClose={onClose} size="full" isOpen={isOpen}>
                        <ModalOverlay />
                        <ModalContent>
                            <ModalCloseButton />
                            <ModalBody>
                                <VStack spacing={2} mt={10}>
                                    <Image
                                        boxShadow="lg"
                                        borderRadius={5}
                                        h={{
                                            base:
                                                images.length === 1
                                                    ? '80vh'
                                                    : '60vh',
                                            sm:
                                                images.length === 1
                                                    ? '80vh'
                                                    : '70vh',
                                        }}
                                        minH="500px"
                                        objectFit="contain"
                                        src={`${process.env.REACT_APP_API_URL}/images/materials/${images[activeIndex].imageName}`}
                                        alt={material.name}
                                    />
                                    {images.length > 1 && (
                                        <Box p={5} w="full">
                                            <Center>
                                                <HStack>
                                                    {images.map((img, i) => {
                                                        return (
                                                            <Image
                                                                cursor="pointer"
                                                                key={`${img.imageName}`}
                                                                src={`${process.env.REACT_APP_API_URL}/images/materials/${img.imageName}`}
                                                                h="100px"
                                                                maxW="300px"
                                                                boxShadow="sm"
                                                                borderBottom="1px solid #ddd"
                                                                onClick={() =>
                                                                    setActiveIndex(
                                                                        i
                                                                    )
                                                                }
                                                            />
                                                        )
                                                    })}
                                                </HStack>
                                            </Center>
                                        </Box>
                                    )}
                                    <Box
                                        position={{
                                            base: 'relative',
                                            md: 'fixed',
                                        }}
                                        right={{ base: 'initial', md: 10 }}
                                        bottom={{ base: 'initial', md: 2 }}
                                    >
                                        <Button
                                            onClick={onClose}
                                            variant="ghost"
                                            colorScheme="yellow"
                                        >
                                            <Kbd
                                                display={{
                                                    base: 'none',
                                                    md: 'block',
                                                }}
                                                mr={2}
                                            >
                                                Esc
                                            </Kbd>
                                            {t('modal.close')}
                                        </Button>
                                    </Box>
                                </VStack>
                            </ModalBody>
                        </ModalContent>
                    </Modal>
                </>
            )}
        </Box>
    )
}

export default MaterialPictures
