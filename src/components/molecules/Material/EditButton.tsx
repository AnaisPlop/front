import { Button } from '@chakra-ui/react'
import { generatePath, Link } from 'react-router-dom'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Material, MaterialLegacy } from '../../../types'

const EditButton = ({ material }: { material: Material | MaterialLegacy }) => {
    const { t } = useTranslation()
    return (
        <Link
            to={generatePath(`/my/materials/:id/edit`, {
                id: material?._id,
            })}
        >
            <Button
                flex={1}
                width="full"
                fontSize="sm"
                colorScheme="green"
                _hover={{
                    transform: 'translateY(2px)',
                    boxShadow: 'lg',
                }}
            >
                {t('pooling.edit.button')}
            </Button>
        </Link>
    )
}

export default EditButton
