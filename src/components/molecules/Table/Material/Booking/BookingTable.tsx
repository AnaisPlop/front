import {
    Alert,
    Button,
    ButtonGroup,
    Flex,
    Image,
    Link as CLink,
    Table,
    TableContainer,
    Tbody,
    Td,
    Text,
    Th,
    Thead,
    Tr,
    VStack,
} from '@chakra-ui/react'
import moment from 'moment'
import { FaChevronRight } from 'react-icons/fa'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import BookingStatusActionButton from '../../../../atoms/Button/material/booking/BookingStatusActionButton'
import { MaterialBookingEdge } from '../../../../../types'
import moneyFormat from '../../../../../utils/money'
import randomTrans from '../../../../../utils/randomTrans'
import transformBackendUri from '../../../../../utils/transformBackendUri'

const BookingTable = ({
    materialBookings,
}: {
    materialBookings: [MaterialBookingEdge]
}) => {
    const { t, i18n } = useTranslation()
    return (
        (materialBookings.length > 0 && (
            <TableContainer w="100%" my={25}>
                <Table variant="striped" colorScheme="orange" size="sm">
                    <Thead>
                        <Tr>
                            <Th>{t('pooling.booking.index.table.material')}</Th>
                            <Th>{t('pooling.booking.index.table.periods')}</Th>
                            <Th>{t('pooling.booking.index.table.pricing')}</Th>
                            <Th>{t('pooling.booking.index.table.status')}</Th>
                            <Th>{t('pooling.booking.index.table.action')}</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {materialBookings.map(
                            ({ node: booking }: MaterialBookingEdge) => {
                                const picture =
                                    booking.material.images.edges.length > 0
                                        ? transformBackendUri(
                                              `/images/materials/${booking.material.images.edges[0].node.imageName}`
                                          )
                                        : 'https://images.unsplash.com/photo-1518051870910-a46e30d9db16?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80'

                                return (
                                    <Tr key={`booking-${booking._id}`}>
                                        <Td>
                                            <Flex alignItems="center" gap={3}>
                                                <Image
                                                    boxSize="30px"
                                                    objectFit="cover"
                                                    src={picture}
                                                    alt={booking.material.name}
                                                />
                                                <VStack>
                                                    <CLink>
                                                        <Link
                                                            to={`/materials/${booking.material._id}`}
                                                        >
                                                            {
                                                                booking.material
                                                                    .name
                                                            }
                                                        </Link>
                                                    </CLink>
                                                    <Text>
                                                        {`${booking.user.fullname}`}
                                                    </Text>
                                                </VStack>
                                            </Flex>
                                        </Td>
                                        <Td>
                                            <ul>
                                                {booking?.periods.edges.map(
                                                    ({ node: period }) => {
                                                        return (
                                                            <li
                                                                key={`periods-${booking._id}-${period._id}`}
                                                            >
                                                                {(moment(
                                                                    period.startDate
                                                                ).format(
                                                                    'L'
                                                                ) !==
                                                                    moment(
                                                                        period.endDate
                                                                    ).format(
                                                                        'L'
                                                                    ) &&
                                                                    t(
                                                                        'pooling.show.booking.summary.periodItem.range.label',
                                                                        {
                                                                            startDate:
                                                                                moment(
                                                                                    period.startDate
                                                                                ).format(
                                                                                    'LL'
                                                                                ),
                                                                            endDate:
                                                                                moment(
                                                                                    period.endDate
                                                                                ).format(
                                                                                    'LL'
                                                                                ),
                                                                            price: period.price,
                                                                        }
                                                                    )) ||
                                                                    t(
                                                                        'pooling.show.booking.summary.periodItem.singleDay.label',
                                                                        {
                                                                            day: moment(
                                                                                period.startDate
                                                                            ).format(
                                                                                'LL'
                                                                            ),
                                                                        }
                                                                    )}
                                                            </li>
                                                        )
                                                    }
                                                )}
                                            </ul>
                                        </Td>
                                        <Td>
                                            {(booking.price &&
                                                booking.price !== 0 &&
                                                t(
                                                    'pooling.show.booking.summary.periodItem.price',
                                                    {
                                                        price: moneyFormat(
                                                            booking.price,
                                                            i18n.language
                                                        ),
                                                    }
                                                )) ||
                                                randomTrans(
                                                    t,
                                                    'pooling.show.pricing.free',
                                                    9
                                                )}
                                        </Td>
                                        <Td>
                                            {t('pooling.booking.status', {
                                                context: booking.status,
                                            })}
                                        </Td>
                                        <Td>
                                            <ButtonGroup>
                                                <BookingStatusActionButton
                                                    _booking={booking}
                                                />
                                                <Link
                                                    to={`/my/bookings/${booking._id}`}
                                                >
                                                    <Button
                                                        variant="ghost"
                                                        colorScheme="red"
                                                    >
                                                        <FaChevronRight />
                                                    </Button>
                                                </Link>
                                            </ButtonGroup>
                                        </Td>
                                    </Tr>
                                )
                            }
                        )}
                    </Tbody>
                </Table>
            </TableContainer>
        )) || <Alert>{t('pooling.booking.index.empty')}</Alert>
    )
}

export default BookingTable
