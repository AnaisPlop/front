import { Box, Heading, SimpleGrid, Stack } from '@chakra-ui/react'
import * as React from 'react'
import { ReactElement } from 'react'
import Feature from './Feature'
import Testimonial from './Testimonial'

type Props = {
    button?: ReactElement
    features: {
        content: string
        icon: ReactElement
        title: string
    }[]
    testimonials: {
        author: {
            avatar: string
            location: string
            name: string
        }
        content: string
    }[]
    title: string
}

const DarkWithTestimonial = ({
    testimonials,
    features,
    title,
    button,
}: Props) => {
    return (
        <Box as="section" pb="24">
            <Box bg="gray.800" color="white" pt="24" pb="12rem">
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <Stack
                        spacing="10"
                        direction={{ base: 'column', lg: 'row' }}
                        align={{ base: 'flex-start', lg: 'center' }}
                        justify="space-between"
                    >
                        <Heading
                            size="2xl"
                            lineHeight="short"
                            fontWeight="extrabold"
                            maxW={{ base: 'unset', lg: '800px' }}
                        >
                            {title}
                        </Heading>
                        {button}
                    </Stack>
                    <SimpleGrid
                        columns={{ base: 1, md: 2, lg: 4 }}
                        spacing={{ base: '12', md: '8', lg: '2' }}
                        mt={{ base: '12', md: '20' }}
                    >
                        {features.map(
                            ({ icon, title: featureTitle, content }) => (
                                <Feature
                                    key={title}
                                    icon={icon}
                                    title={featureTitle}
                                >
                                    {content}
                                </Feature>
                            )
                        )}
                    </SimpleGrid>
                </Box>
            </Box>
            <Box mt="-24">
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <SimpleGrid spacing="14" columns={{ base: 1, lg: 2 }}>
                        {testimonials.map(({ author, content }) => (
                            <Testimonial
                                key={author.name}
                                image={author.avatar}
                                name={author.name}
                                role={author.location}
                            >
                                {content}
                            </Testimonial>
                        ))}
                    </SimpleGrid>
                </Box>
            </Box>
        </Box>
    )
}

export default DarkWithTestimonial
