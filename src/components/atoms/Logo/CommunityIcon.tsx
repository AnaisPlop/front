import * as React from 'react'
import logo from './community.svg'

const CommunityIcon = ({
    width,
    height,
}: {
    width: number
    height: number
}) => <img src={logo} alt="Community" width={width} height={height} />

export default CommunityIcon
