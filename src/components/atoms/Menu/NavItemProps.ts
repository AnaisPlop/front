import { FlexProps } from '@chakra-ui/react'
import { IconType } from 'react-icons'
import React from 'react'
import { SubLinkItemProps } from '../../layout/LayoutProps'

export declare interface NavItemProps extends FlexProps {
    icon?: IconType
    to?: string
    subItems?: SubLinkItemProps[]
    children: React.ReactNode
}
