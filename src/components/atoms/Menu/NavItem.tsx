import { Link, LinkProps, To, useLocation } from 'react-router-dom'
import {
    Flex,
    Icon,
    Link as CLink,
    Menu,
    MenuButton,
    MenuItem,
    MenuList,
} from '@chakra-ui/react'
import React, { ForwardRefExoticComponent, RefAttributes } from 'react'
import { NavItemProps } from './NavItemProps'
import isUrl, { isPathCurrent } from '../../../utils/isUrl'

const NavItem = ({
    icon,
    to,
    display,
    subItems,
    children,
    ...rest
}: NavItemProps) => {
    const href = (!subItems && to) || '#'
    let linkProps: {
        isExternal?: boolean
        to: To
        href?: string
        as?: ForwardRefExoticComponent<
            LinkProps & RefAttributes<HTMLAnchorElement>
        >
    } = { to: href }
    if (to && isUrl(to)) {
        linkProps = { ...linkProps, href, isExternal: true }
    } else {
        linkProps = {
            to: href,
            as: Link,
        }
    }
    const location = useLocation()

    return (
        <CLink
            style={{ textDecoration: 'none' }}
            {...linkProps}
            display={display}
            zIndex={2}
        >
            <Flex
                align="center"
                p="6"
                mx="4"
                role="group"
                cursor="pointer"
                borderBottomWidth="5px"
                borderBottomColor={
                    isPathCurrent(href, location.pathname)
                        ? 'yellow.500'
                        : 'transparent'
                }
                _hover={{
                    borderBottomColor: 'yellow.500',
                }}
                {...rest}
            >
                {icon && (
                    <Icon
                        display={{ base: 'none', lg: 'flex' }}
                        mr="4"
                        fontSize="16"
                        _groupHover={{
                            color: 'yellow.500',
                        }}
                        as={icon}
                    />
                )}
                {(subItems && (
                    <Menu>
                        <MenuButton>{children}</MenuButton>
                        <MenuList ml="-60px" mt="21px" borderTopRadius={0}>
                            {subItems.map((item) => (
                                <MenuItem
                                    w="100%"
                                    key={item.name}
                                    as={Link}
                                    to={item.to ?? '#'}
                                >
                                    {item.name}
                                </MenuItem>
                            ))}
                        </MenuList>
                    </Menu>
                )) ||
                    children}
            </Flex>
        </CLink>
    )
}

export default NavItem
