import {
    Box,
    Flex,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Input,
    Select,
} from '@chakra-ui/react'
import React from 'react'
import { FieldError, UseFormRegisterReturn } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { getCountries, getCountryCallingCode } from 'react-phone-number-input'
import { isValid, getName, registerLocale } from 'i18n-iso-countries'
import en from 'i18n-iso-countries/langs/en.json'
import fr from 'i18n-iso-countries/langs/fr.json'
import RequiredAsterisk from './RequiredAsterisk'

interface PhoneNumberProps {
    name: string
    countryCodeRegister: UseFormRegisterReturn<'phoneNumberObject.countryCode'>
    nationalNumberRegister: UseFormRegisterReturn<'phoneNumberObject.nationalNumber'>
    errors:
        | {
              countryCode?: FieldError | undefined
              nationalNumber?: FieldError | undefined
          }
        | undefined
    isRequired?: boolean
}

const PhoneNumberInput = ({
    name,
    countryCodeRegister,
    nationalNumberRegister,
    errors,
    isRequired = false,
}: PhoneNumberProps) => {
    const { t, i18n } = useTranslation()
    const countries = getCountries()
    registerLocale(en)
    registerLocale(fr)

    return (
        <FormControl id={name} isInvalid={errors !== undefined}>
            <FormLabel>
                {t('register.form.phoneNumber.label')}{' '}
                {isRequired && <RequiredAsterisk />}
            </FormLabel>
            <Flex>
                <Select
                    defaultValue={getCountryCallingCode('FR')}
                    isRequired={isRequired}
                    {...countryCodeRegister}
                    w="6rem"
                >
                    {countries.map(
                        (code) =>
                            isValid(code) && (
                                <option
                                    key={code}
                                    value={getCountryCallingCode(code)}
                                >
                                    {getName(code, i18n.language)}
                                </option>
                            )
                    )}
                </Select>
                <Box flexGrow={1} ml={1}>
                    <Input
                        type="text"
                        placeholder={t('register.form.phoneNumber.placeholder')}
                        isRequired={isRequired}
                        {...nationalNumberRegister}
                    />
                </Box>
            </Flex>
            {errors !== undefined && (
                <FormErrorMessage>
                    {errors.nationalNumber?.message}
                </FormErrorMessage>
            )}
        </FormControl>
    )
}

export default PhoneNumberInput
