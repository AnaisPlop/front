import * as React from 'react'
import {
    Button as ChakraButton,
    forwardRef,
    ButtonProps,
} from '@chakra-ui/react'
import { Link } from 'react-router-dom'
import { ReactNode } from 'react'

export type Props = {
    link?: string
    children: ReactNode
} & ButtonProps

const Button = forwardRef(
    ({ borderRadius = 'full', children, link, ...otherProps }: Props, ref) => {
        return (
            <ChakraButton borderRadius={borderRadius} ref={ref} {...otherProps}>
                {(link && <Link to={link}>{children}</Link>) || children}
            </ChakraButton>
        )
    }
)
export const PrimaryButton = forwardRef(
    ({ children, ...otherProps }: Props, ref) => {
        return (
            <Button
                bg="yellow.400"
                colorScheme="yellow"
                {...otherProps}
                ref={ref}
            >
                {children}
            </Button>
        )
    }
)
export const InfoButton = forwardRef(
    ({ children, ...otherProps }: Props, ref) => {
        return (
            <Button {...otherProps} colorScheme="blue" ref={ref}>
                {children}
            </Button>
        )
    }
)

export const SuccessButton = forwardRef(
    ({ children, ...otherProps }: Props, ref) => {
        return (
            <Button
                bg="green.400"
                colorScheme="green"
                {...otherProps}
                ref={ref}
            >
                {children}
            </Button>
        )
    }
)

export const RainbowButon = forwardRef(
    (
        {
            bg = 'yellow.400',
            children,
            colorScheme = 'yellow',
            _hover = {
                bgGradient: 'linear(to-l, purple.400, pink.400)',
                color: 'yellow',
                transform: 'translateY(2px)',
                boxShadow: 'lg',
            },
            ...otherProps
        }: Props,
        ref
    ) => {
        return (
            <Button
                {...{ bg, colorScheme }}
                {...otherProps}
                _hover={_hover}
                ref={ref}
            >
                {children}
            </Button>
        )
    }
)

export default Button
