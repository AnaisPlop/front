import React from 'react'
import { useApolloClient, useMutation, useQuery } from '@apollo/client'
import { ButtonGroup, Spinner } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import { MaterialBooking } from '../../../../../types'
import {
    changeStatusMaterialBooking,
    findMaterialBookingById,
} from '../../../../../repositories/Material/MaterialBookingRepository'
import Button from '../../index'

const BookingStatusActionButton = ({
    _booking,
}: {
    _booking: MaterialBooking
}) => {
    const { t } = useTranslation()
    const client = useApolloClient()
    const { loading, data } = useQuery(findMaterialBookingById, {
        variables: {
            id: _booking.id,
        },
    })
    let booking: MaterialBooking | null = null
    if (!loading) {
        booking = data.materialBooking
    }
    const [changeStatus] = useMutation(changeStatusMaterialBooking, {
        update() {
            client.resetStore()
        },
    })
    const onAction = (id: String, action: String) => {
        const variables = { id, transition: action }
        changeStatus({
            variables,
        }).then((r) => {
            if (r.data.changeStatusMaterialBooking.materialBooking === null) {
                toast.error(
                    t('pooling.booking.updateStatus.toast', {
                        context: 'error',
                        action: t('pooling.booking.action', {
                            context: action,
                        }).toLowerCase(),
                    })
                )
                return
            }
            toast(
                t('pooling.booking.updateStatus.toast', {
                    context: 'success',
                })
            )
        })
    }

    return (
        (booking?.statusTransitionAvailables && (
            <ButtonGroup alignSelf="center">
                {booking?.statusTransitionAvailables.map((action) => (
                    <Button
                        variant={
                            ['confirm', 'apply'].includes(action)
                                ? 'solid'
                                : 'outline'
                        }
                        colorScheme={
                            ['confirm', 'apply'].includes(action)
                                ? 'green'
                                : 'red'
                        }
                        key={`action-${action}`}
                        onClick={() => booking && onAction(booking.id, action)}
                    >
                        {t('pooling.booking.action', { context: action })}
                    </Button>
                ))}
            </ButtonGroup>
        )) || <Spinner />
    )
}

export default BookingStatusActionButton
