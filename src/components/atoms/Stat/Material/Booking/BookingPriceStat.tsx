import {
    List,
    ListItem,
    Stat,
    StatHelpText,
    StatNumber,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import moneyFormat from '../../../../../utils/money'
import randomTrans from '../../../../../utils/randomTrans'
import Card from '../../../../molecules/Cards/Card'

const BookingPriceStat = ({
    price,
    periods,
}: {
    price: number
    periods: {
        _id: string
        endDate: string | null
        startDate: string | null
    }[]
}) => {
    const { t, i18n } = useTranslation()

    return (
        <Card>
            <Stat>
                <StatNumber>
                    {(price &&
                        price !== 0 &&
                        t('pooling.show.booking.summary.periodItem.price', {
                            price: moneyFormat(price, i18n.language),
                        })) ||
                        randomTrans(t, 'pooling.show.pricing.free', 9)}
                </StatNumber>
                <StatHelpText>
                    <List>
                        {periods.map(({ endDate, startDate, _id }) => {
                            return (
                                <ListItem key={`period-${_id}`}>
                                    {(moment(startDate).format('L') !==
                                        moment(endDate).format('L') &&
                                        t(
                                            'pooling.show.booking.summary.periodItem.range.label',
                                            {
                                                startDate:
                                                    moment(startDate).format(
                                                        'LL'
                                                    ),
                                                endDate:
                                                    moment(endDate).format(
                                                        'LL'
                                                    ),
                                                price,
                                            }
                                        )) ||
                                        t(
                                            'pooling.show.booking.summary.periodItem.singleDay.label',
                                            {
                                                day: moment(startDate).format(
                                                    'LL'
                                                ),
                                            }
                                        )}
                                </ListItem>
                            )
                        })}
                    </List>
                </StatHelpText>
            </Stat>
        </Card>
    )
}

export default BookingPriceStat
