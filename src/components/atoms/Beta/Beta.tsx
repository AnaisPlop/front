import { Badge } from '@chakra-ui/react'

const Beta = () => {
    return <Badge colorScheme="yellow">BETA</Badge>
}
export const Wip = () => {
    return <Badge colorScheme="orange">🚧 WIP</Badge>
}

export default Beta
