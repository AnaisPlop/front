import Lottie from 'react-lottie'
import React from 'react'
import animationData from '../../../lotties/paint-color-loader.json'

const Loader = ({ height = 400, width = 400 }) => {
    return (
        <Lottie
            options={{
                loop: true,
                autoplay: true,
                animationData,
                rendererSettings: {
                    preserveAspectRatio: 'xMidYMid slice',
                },
            }}
            height={width}
            width={height}
        />
    )
}

export default Loader
