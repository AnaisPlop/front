import * as React from 'react'
import { Text } from '@chakra-ui/react'
import { Trans } from 'react-i18next'

type Props = { children: string; color: string }

const EmphasisText = ({ children, color }: Props) => (
    <Trans
        i18nKey={children}
        components={{
            em: <Text as="span" color={color} />,
            xs: <Text as="span" fontSize="xs" />,
            md: <Text as="span" fontSize="md" />,
            lg: <Text as="span" fontSize="lg" />,
            xl: <Text as="span" fontSize="xl" />,
            sub: <sub />,
        }}
    />
)

export default EmphasisText
