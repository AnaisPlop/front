import { Badge, Heading, Text } from '@chakra-ui/react'
import * as React from 'react'
import { User } from '../../../types'

const Blaze = ({ roles, fullname }: User) => (
    <Heading fontSize="2xl" fontFamily="body">
        {(roles.includes('ROLE_MODERATOR') && (
            <Text
                bgGradient="linear(to-l, #7928CA, #FF0080)"
                bgClip="text"
                fontWeight="extrabold"
            >
                {fullname}{' '}
                <Badge ml="1" colorScheme="green">
                    Admin
                </Badge>
            </Text>
        )) ||
            fullname}
    </Heading>
)

export default Blaze
