import {
    useColorModeValue,
    Heading as ChakraHeading,
    HeadingProps,
} from '@chakra-ui/react'

export type Props = {} & HeadingProps

const Heading = ({ children, ...otherProps }: Props) => {
    return <ChakraHeading {...otherProps}>{children}</ChakraHeading>
}

export const H1 = (props: Props) => {
    const { children } = props

    return (
        <Heading
            as="h1"
            color={useColorModeValue('gray.600', 'white')}
            lineHeight={1.1}
            fontSize={{
                base: 'xl',
                sm: '2xl',
                md: '3xl',
            }}
            {...props}
        >
            {children}
        </Heading>
    )
}
export const H2 = (props: Props) => {
    const { children } = props

    return (
        <Heading
            as="h2"
            color={useColorModeValue('yellow.400', 'yellow.200')}
            lineHeight={1.1}
            fontSize={{
                base: 'lg',
                sm: 'xl',
                md: '2xl',
            }}
            {...props}
        >
            {children}
        </Heading>
    )
}
export const H3 = (props: Props) => {
    const { children } = props

    return (
        <Heading
            as="h3"
            color={useColorModeValue('yellow.400', 'yellow.200')}
            lineHeight={1}
            fontSize={{
                base: 'md',
                sm: 'lg',
                md: 'xl',
            }}
            {...props}
        >
            {children}
        </Heading>
    )
}

export default Heading
