import { Box, Heading, SimpleGrid, Stack } from '@chakra-ui/react'
import * as React from 'react'
import { BiHappyBeaming, BiMap, BiMoney } from 'react-icons/bi'
import { FaHandsHelping } from 'react-icons/fa'
import { CTAButton } from './CTAButton'
import { Feature } from './Feature'
import { Testimonial } from './Testimonial'

const DarkWithTestimonial = () => {
    return (
        <Box as="section" pb="24">
            <Box bg="gray.800" color="white" pt="24" pb="12rem">
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <Stack
                        spacing="10"
                        direction={{ base: 'column', lg: 'row' }}
                        align={{ base: 'flex-start', lg: 'center' }}
                        justify="space-between"
                    >
                        <Heading
                            size="2xl"
                            lineHeight="short"
                            fontWeight="extrabold"
                            maxW={{ base: 'unset', lg: '800px' }}
                        >
                            Rejoins la communauté !
                        </Heading>
                        <CTAButton w={{ base: 'full', md: 'auto' }}>
                            Inscription
                        </CTAButton>
                    </Stack>
                    <SimpleGrid
                        columns={{ base: 1, md: 2, lg: 4 }}
                        spacing={{ base: '12', md: '8', lg: '2' }}
                        mt={{ base: '12', md: '20' }}
                    >
                        <Feature
                            icon={<BiMap />}
                            title="Ta voisine est déjà parmi nous !"
                        >
                            Communo ne fonctionne que s'il y'a du monde, alors
                            une fois dedans, il faut aller en parler partout
                            autour de chez toi !
                        </Feature>
                        <Feature icon={<BiMoney />} title="Désolé Merlin">
                            Fini d'aller claquer tes économies au magasin de
                            bricolage. Ça ne va pas te manquer, je te le promets
                            !
                        </Feature>
                        <Feature
                            icon={<FaHandsHelping />}
                            title="Les conseils inclus"
                        >
                            C'est logique, quand on emprunte du matos, on reçoit
                            les conseils avisés de son ou sa propriétaire (et
                            c'est cool).
                        </Feature>
                        <Feature
                            icon={<BiHappyBeaming />}
                            title="Des rencontres !"
                        >
                            Je dis pas ton âme-soeur mais c'est pas exclu que tu
                            te fasses des ami·e·s car il n'y a que des perles
                            comme toi qui souhaitent jouer collectif ici !
                        </Feature>
                    </SimpleGrid>
                </Box>
            </Box>
            <Box mt="-24">
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <SimpleGrid spacing="14" columns={{ base: 1, lg: 2 }}>
                        <Testimonial
                            image={`${process.env.REACT_APP_API_URL}/images/users/fixture_cyril.jpg`}
                            name="Cyril Allain"
                            role="Gétigné, 44"
                        >
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat.
                        </Testimonial>
                        <Testimonial
                            image="https://images.unsplash.com/photo-1589729482945-ca6f3a235f7a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2550&q=80"
                            name="Laeticia Jaunet"
                            role="Cugand, 85"
                        >
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco.
                        </Testimonial>
                        <Testimonial
                            image={`${process.env.REACT_APP_API_URL}/images/users/fixture_leny.jpg`}
                            name="Leny Bernard"
                            role="Gétigné, 44"
                        >
                            Très heureux de pouvoir enfin ouvrir mon atelier à
                            la communauté et de pouvoir emprunter du bon matos,
                            ça fait du bien de jouer collectif et de se faire
                            confiance !
                        </Testimonial>
                        <Testimonial
                            image={`${process.env.REACT_APP_API_URL}/images/users/fixture_marion.jpg`}
                            name="Leny Bernard"
                            role="Gétigné, 44"
                        >
                            Le réflexe est arrivé très vite et j'espère que de
                            plus en plus de monde va s'y mettre par chez moi,
                            c'est tellement logique de mutualiser les
                            équipements plutôt qu'on achète tous, chacun comme
                            des cons, les mêmes produits moyenne gamme... !
                        </Testimonial>
                    </SimpleGrid>
                </Box>
            </Box>
        </Box>
    )
}

export default DarkWithTestimonial
