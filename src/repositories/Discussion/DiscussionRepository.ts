import { gql } from '@apollo/client'

const baseDiscussionQueryAttributes = gql`
    {
        id
        _id
        messages {
            edges {
                node {
                    id
                    _id
                    content
                    author {
                        id
                        _id
                        firstname
                        lastname
                        fullname
                        avatar {
                            contentUrl
                        }
                    }
                    createdAt
                }
            }
        }
    }
`

// eslint-disable-next-line import/prefer-default-export
export const addMessageDiscussion = gql`
    mutation addMessageDiscussion($id: ID!, $message: String) {
        addMessageDiscussion(input: { id: $id, message: $message }) {
            discussion ${baseDiscussionQueryAttributes}
        }
    }
`
