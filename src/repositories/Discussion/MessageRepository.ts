import { gql } from '@apollo/client'

export const getMessages = gql`
    query getMessage {
        messages {
            edges {
                node {
                    id
                    _id
                    content
                    author {
                        id
                        _id
                        firstname
                        lastname
                        fullname
                        avatar {
                            contentUrl
                        }
                    }
                    createdAt
                    discussion {
                        id
                    }
                }
            }
        }
    }
`

export const deleteMessage = gql`
    mutation DeleteMessage($id: ID!) {
        deleteMessage(input: { id: $id }) {
            clientMutationId
        }
    }
`
