import { gql } from '@apollo/client'

export const findMaterialBookings = gql`
    query GetMaterialBookings {
        materialBookings {
            edges {
                node {
                    id
                    _id
                    startDate
                    endDate
                    status
                    price
                    user {
                        id
                        _id
                        firstname
                        lastname
                        fullname
                        email
                        roles
                        avatar {
                            contentUrl
                        }
                    }
                    periods {
                        edges {
                            node {
                                id
                                _id
                                startDate
                                endDate
                                price
                            }
                        }
                    }
                    material {
                        id
                        _id
                        name
                        model
                        brand
                        reference
                        createdAt
                        updatedAt
                        images {
                            edges {
                                node {
                                    _id
                                    imageName
                                    imageSize
                                }
                            }
                        }
                        owner {
                            _id
                            firstname
                            lastname
                            email
                            phoneNumber
                            avatar {
                                contentUrl
                            }
                            streetAddress
                            city
                            roles
                            gender
                            materials {
                                paginationInfo {
                                    totalCount
                                }
                                collection {
                                    _id
                                    name
                                }
                            }
                            circles {
                                collection {
                                    id
                                    _id
                                    name
                                    description
                                    logoName
                                    logoSize
                                    city
                                }
                            }
                            averageRatingScore
                            ratings {
                                totalCount
                            }
                        }
                    }
                }
            }
        }
    }
`

export const estimateMaterialBooking = gql`
    mutation estimate(
        $materialId: String
        $startDate: String
        $endDate: String
    ) {
        estimateMaterialBooking(
            input: {
                materialId: $materialId
                startDate: $startDate
                endDate: $endDate
            }
        ) {
            materialBooking {
                id
                price
                periods {
                    edges {
                        node {
                            id
                            startDate
                            endDate
                            price
                        }
                    }
                }
            }
        }
    }
`

export const findMaterialBookingsForApplicant = gql`
    query GetMaterialBookingsForApplicant($id: String, $statuses: [String]) {
        materialBookings(user: $id, status_list: $statuses) {
            edges {
                node {
                    id
                    _id
                    startDate
                    endDate
                    status
                    price
                    user {
                        id
                        _id
                        firstname
                        lastname
                        fullname
                        email
                        roles
                        avatar {
                            contentUrl
                        }
                    }
                    periods {
                        edges {
                            node {
                                id
                                _id
                                startDate
                                endDate
                                price
                            }
                        }
                    }
                    price
                    material {
                        id
                        _id
                        name
                        model
                        brand
                        reference
                        createdAt
                        updatedAt
                        images {
                            edges {
                                node {
                                    _id
                                    imageName
                                    imageSize
                                }
                            }
                        }
                        owner {
                            _id
                            firstname
                            lastname
                            email
                            phoneNumber
                            avatar {
                                contentUrl
                            }
                            streetAddress
                            city
                            roles
                            gender
                            materials {
                                paginationInfo {
                                    totalCount
                                }
                                collection {
                                    _id
                                    name
                                }
                            }
                            circles {
                                collection {
                                    id
                                    _id
                                    name
                                    description
                                    logoName
                                    logoSize
                                    city
                                }
                            }
                            averageRatingScore
                            ratings {
                                totalCount
                            }
                        }
                    }
                }
            }
        }
    }
`
export const findMaterialBookingsForOwner = gql`
    query GetMaterialBookingsForOwner($id: String, $statuses: [String]) {
        materialBookings(material_owner: $id, status_list: $statuses) {
            edges {
                node {
                    id
                    _id
                    startDate
                    endDate
                    status
                    price
                    user {
                        id
                        _id
                        firstname
                        lastname
                        fullname
                        email
                        roles
                        avatar {
                            contentUrl
                        }
                    }
                    periods {
                        edges {
                            node {
                                id
                                _id
                                startDate
                                endDate
                                price
                            }
                        }
                    }
                    material {
                        id
                        _id
                        name
                        model
                        brand
                        reference
                        createdAt
                        updatedAt
                        images {
                            edges {
                                node {
                                    _id
                                    imageName
                                    imageSize
                                }
                            }
                        }
                        owner {
                            _id
                            firstname
                            lastname
                            email
                            phoneNumber
                            avatar {
                                contentUrl
                            }
                            streetAddress
                            city
                            roles
                            gender
                            materials {
                                paginationInfo {
                                    totalCount
                                }
                                collection {
                                    _id
                                    name
                                }
                            }
                            circles {
                                collection {
                                    id
                                    _id
                                    name
                                    description
                                    logoName
                                    logoSize
                                    city
                                }
                            }
                            averageRatingScore
                            ratings {
                                totalCount
                            }
                        }
                    }
                }
            }
        }
    }
`

const baseMaterialBookingQueryAttributes = gql`
    {
        id
        _id
        startDate
        endDate
        status
        statusTransitionAvailables
        price
        discussion {
            id
            _id
            messages {
                edges {
                    node {
                        id
                        _id
                        content
                        author {
                            id
                            _id
                            firstname
                            lastname
                            fullname
                            avatar {
                                contentUrl
                            }
                        }
                        createdAt
                    }
                }
            }
        }
        user {
            _id
            firstname
            lastname
            fullname
            email
            phoneNumber
            avatar {
                contentUrl
            }
            city
            roles
            gender
            materials {
                paginationInfo {
                    totalCount
                }
                collection {
                    _id
                    name
                }
            }
            circles {
                collection {
                    id
                    _id
                    name
                    description
                    logoName
                    logoSize
                    city
                }
            }
            averageRatingScore
            ratings {
                totalCount
            }
        }
        material {
            id
            _id
            name
            model
            brand
            reference
            createdAt
            updatedAt
            images {
                edges {
                    node {
                        _id
                        imageName
                        imageSize
                    }
                }
            }
            owner {
                _id
                firstname
                lastname
                fullname
                email
                phoneNumber
                avatar {
                    contentUrl
                }
                streetAddress
                city
                roles
                gender
                materials {
                    paginationInfo {
                        totalCount
                    }
                    collection {
                        _id
                        name
                    }
                }
                circles {
                    collection {
                        id
                        _id
                        name
                        description
                        logoName
                        logoSize
                        city
                    }
                }
                averageRatingScore
                ratings {
                    totalCount
                }
            }
        }
        periods {
            edges {
                node {
                    id
                    startDate
                    endDate
                    price
                }
            }
        }
    }
`
export const findMaterialBookingById = gql`
    query GetMaterialBooking($id: ID!) {
        materialBooking(id: $id) ${baseMaterialBookingQueryAttributes}
    }
`
export const changeStatusMaterialBooking = gql`
    mutation ChangeStatusMaterialBooking($id: ID!, $transition: String) {
        changeStatusMaterialBooking(
            input: { id: $id, transition: $transition }
        ) {
            materialBooking ${baseMaterialBookingQueryAttributes}
        }
    }
`
export const applyMaterialBooking = gql`
    mutation changeStatusMaterialBooking($id: ID!, $message: String) {
        changeStatusMaterialBooking(input: { id: $id, transition: "apply", message: $message }) {
            materialBooking ${baseMaterialBookingQueryAttributes}
        }
    }
`
