import { gql } from '@apollo/client'

export const findMaterials = gql`
    query GetMaterials($page: Int, $searchTerms: String) {
        materials(
            page: $page
            name: $searchTerms
            order: [{ createdAt: "DESC" }, { name: "ASC" }]
        ) {
            collection {
                _id
                name
                model
                brand
                reference
                createdAt
                updatedAt
                images {
                    edges {
                        node {
                            _id
                            imageName
                            imageSize
                        }
                    }
                }
            }
            paginationInfo {
                itemsPerPage
                lastPage
                totalCount
            }
        }
    }
`

export const findMaterialById = gql`
    query GetMaterial($id: ID!) {
        material(id: $id) {
            _id
            id
            name
            description
            model
            brand
            reference
            owner {
                _id
                firstname
                lastname
                fullname
                email
                phoneNumber
                avatar {
                    contentUrl
                }
                streetAddress
                city
                roles
                gender
                materials {
                    paginationInfo {
                        totalCount
                    }
                    collection {
                        _id
                        name
                    }
                }
                circles {
                    collection {
                        id
                        _id
                        name
                        description
                        logoName
                        logoSize
                        city
                    }
                }
                averageRatingScore
                ratings {
                    totalCount
                }
            }
            category {
                id
                name
            }
            images {
                edges {
                    node {
                        id
                        imageName
                        imageSize
                    }
                }
            }
            pricings {
                paginationInfo {
                    totalCount
                }
                collection {
                    _id
                    id
                    value
                    period
                    circles {
                        collection {
                            id
                            _id
                            name
                            logoName
                        }
                    }
                }
            }
            bookingPeriods
        }
    }
`

export const deleteMaterialById = gql`
    mutation DeleteMaterial($id: ID!) {
        deleteMaterial(input: { id: $id }) {
            clientMutationId
        }
    }
`

// Pricing

export const createMaterialPricing = gql`
    mutation CreateMaterialPricing(
        $material: String!
        $value: Float!
        $period: Float!
    ) {
        createPricing(
            input: { period: $period, material: $material, value: $value }
        ) {
            pricing {
                id
                value
                period
            }
        }
    }
`

export const updateMaterialPricing = gql`
    mutation UpdateMaterialPricing(
        $id: ID!
        $value: Float!
        $period: Float!
        $circles: [String]
    ) {
        updatePricing(
            input: {
                id: $id
                value: $value
                period: $period
                circles: $circles
            }
        ) {
            pricing {
                id
                value
                period
                circles {
                    collection {
                        id
                    }
                }
            }
        }
    }
`

export const deleteMaterialPricing = gql`
    mutation DeleteMaterialPricing($id: ID!) {
        deletePricing(input: { id: $id }) {
            clientMutationId
        }
    }
`
