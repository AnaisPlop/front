import { gql } from '@apollo/client'

const getCategories = gql`
    query GetCategories {
        materialCategories {
            edges {
                node {
                    _id
                    id
                    name
                }
            }
        }
    }
`

export default getCategories
