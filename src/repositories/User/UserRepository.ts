import { gql } from '@apollo/client'

export const findUserById = gql`
    query GetUser($id: ID!) {
        user(id: $id) {
            id
            _id
            firstname
            lastname
            email
            phoneNumber
            avatar {
                contentUrl
            }
            city
            roles
            gender
            materials {
                paginationInfo {
                    totalCount
                }
                collection {
                    id
                    name
                }
            }
            circles {
                collection {
                    _id
                    id
                    id
                    name
                    description
                    logoName
                    logoSize
                    city
                }
            }
            averageRatingScore
            ratings {
                totalCount
            }
        }
    }
`

export const confirmUser = gql`
    mutation ConfirmUser($id: ID!, $code: String) {
        confirmUser(input: { id: $id, code: $code }) {
            user {
                _id
                id
                firstname
                lastname
                email
                roles
                avatar {
                    contentUrl
                }
                token
                refreshToken
                tokenExpiresAt
            }
        }
    }
`
