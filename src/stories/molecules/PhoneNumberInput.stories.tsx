import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import PhoneNumberInput from '../../components/atoms/Form/PhoneNumberInput'

export default {
    title: 'molecules/Form/PhoneNumberInput',
    component: PhoneNumberInput,
    args: {},
} as ComponentMeta<typeof PhoneNumberInput>

const Template: ComponentStory<typeof PhoneNumberInput> = (args) => (
    <Box maxW="600">
        <PhoneNumberInput {...args} />
    </Box>
)

export const Default = Template.bind({})
Default.args = {}
