import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Box, FormControl, FormLabel } from '@chakra-ui/react'
import GenderForm from '../../components/atoms/Form/GenderForm'
import RequiredAsterisk from '../../components/atoms/Form/RequiredAsterisk'

export default {
    title: 'molecules/Form/GenderForm',
    component: GenderForm,
    args: {},
} as ComponentMeta<typeof GenderForm>

const Template: ComponentStory<typeof GenderForm> = (args) => (
    <Box w={400}>
        <FormControl>
            <FormLabel>
                Gender <RequiredAsterisk />
            </FormLabel>
            <GenderForm {...args} />
        </FormControl>
    </Box>
)

export const Default = Template.bind({})
Default.args = {}
