import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import BookingPriceStat from '../../components/atoms/Stat/Material/Booking/BookingPriceStat'

export default {
    title: 'molecules/BookingPriceStat',
    component: BookingPriceStat,
    args: {},
} as ComponentMeta<typeof BookingPriceStat>

const Template: ComponentStory<typeof BookingPriceStat> = (args) => (
    <BookingPriceStat {...args} />
)

export const Default = Template.bind({})
Default.args = {
    price: 42,
    periods: [
        {
            _id: 'someId',
            endDate: '2022/01/02',
            startDate: '2022/01/01',
        },
    ],
}
