import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Box, FormControl, FormLabel } from '@chakra-ui/react'
import CountryForm from '../../components/atoms/Form/CountryForm'
import RequiredAsterisk from '../../components/atoms/Form/RequiredAsterisk'

export default {
    title: 'molecules/Form/CountryForm',
    component: CountryForm,
    args: {},
} as ComponentMeta<typeof CountryForm>

const Template: ComponentStory<typeof CountryForm> = (args) => (
    <Box w={400}>
        <FormControl>
            <FormLabel>
                Country <RequiredAsterisk />
            </FormLabel>
            <CountryForm {...args} />
        </FormControl>
    </Box>
)

export const Default = Template.bind({})
Default.args = {}
