import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { BiAnalyse } from 'react-icons/bi'
import HeadBar from '../../components/molecules/Menu/HeadBar'

export default {
    title: 'molecules/HeadBar',
    component: HeadBar,
    args: {
        links: [
            {
                name: 'Item',
                icon: BiAnalyse,
                subItems: [
                    {
                        name: 'Subitem 1',
                        to: '#',
                        display: { base: 'none', md: 'flex' },
                    },
                    {
                        name: 'Subitem 2',
                        to: '#',
                        display: { base: 'none', md: 'flex' },
                    },
                ],
            },
        ],
    },
} as ComponentMeta<typeof HeadBar>

const Template: ComponentStory<typeof HeadBar> = (args) => <HeadBar {...args} />

export const Default = Template.bind({})
Default.args = {}
