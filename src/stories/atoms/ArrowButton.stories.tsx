import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import ArrowButton from '../../components/atoms/Button/ArrowButton'
import { PrimaryButton } from '../../components/atoms/Button'

export default {
    title: 'atoms/ArrowButton',
    component: ArrowButton,
    args: {
        button: (
            <PrimaryButton as="button" variant="outline">
                Click
            </PrimaryButton>
        ),
        incentiveLabel: "Let's Communo !",
    },
} as ComponentMeta<typeof ArrowButton>

const Template: ComponentStory<typeof ArrowButton> = (args) => (
    <Box m="100">
        <ArrowButton {...args} />
    </Box>
)

export const Default = Template.bind({})
Default.args = {}
