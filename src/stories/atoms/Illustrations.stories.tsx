import { ComponentMeta, ComponentStory } from '@storybook/react'

import React from 'react'
import Denied from '../../components/atoms/Illustrations/Denied'
import Drill from '../../components/atoms/Illustrations/Drill'
import Hippopotame from '../../components/atoms/Illustrations/Hippopotame'
import Pig from '../../components/atoms/Illustrations/Pig'
import Stamp from '../../components/atoms/Illustrations/Stamp'
import HandExchange from '../../components/atoms/Illustrations/HandExchange'
import ExchangeMailCloud from '../../components/atoms/Illustrations/ExchangeMailCloud'
import Heart from '../../components/atoms/Illustrations/Heart'

export default {
    title: 'atoms/Illustration',
    component: Denied,
    args: {},
} as ComponentMeta<typeof Denied>

const DeniedTemplate: ComponentStory<typeof Denied> = () => (
    <Denied height="300px" />
)

export const DeniedIllustration = DeniedTemplate.bind({})
DeniedIllustration.args = {}
const HeartTemplate: ComponentStory<typeof Heart> = () => (
    <Heart height="400px" />
)
export const HeartIllustration = HeartTemplate.bind({})
HeartIllustration.args = {}

const HandExchangeTemplate: ComponentStory<typeof HandExchange> = () => (
    <HandExchange height="50px" mt={150} />
)
export const HandExchangeIllustration = HandExchangeTemplate.bind({})
HandExchangeIllustration.args = {}

const ExchangeMailCloudTemplate: ComponentStory<
    typeof ExchangeMailCloud
> = () => <ExchangeMailCloud height="450px" mt={50} />

export const ExchangeMailCloudIllustration = ExchangeMailCloudTemplate.bind({})
ExchangeMailCloudIllustration.args = {}

const DrillTemplate: ComponentStory<typeof Drill> = () => <Drill mt={50} />
export const DrillIllustration = DrillTemplate.bind({})
DrillIllustration.args = {}

const HippopotameTemplate: ComponentStory<typeof Hippopotame> = () => (
    <Hippopotame mt={50} />
)
export const HippopotameIllustration = HippopotameTemplate.bind({})
HippopotameIllustration.args = {}

const PigTemplate: ComponentStory<typeof Pig> = () => <Pig mt={50} />
export const PigIllustration = PigTemplate.bind({})
PigIllustration.args = {}

const StampTemplate: ComponentStory<typeof Stamp> = () => <Stamp mt={50} />
export const StampIllustration = StampTemplate.bind({})
StampIllustration.args = {}
