import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import Logo from '../../components/atoms/Logo'

export default {
    title: 'atoms/Logo',
    component: Logo,
    args: {},
} as ComponentMeta<typeof Logo>

const Template: ComponentStory<typeof Logo> = (args) => (
    <Box maxW="500">
        <Logo {...args} />
    </Box>
)

export const Default = Template.bind({})
Default.args = {}
