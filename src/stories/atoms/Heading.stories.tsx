import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Heading as ChakraHeading } from '@chakra-ui/react'
import Heading, { H1, H2, H3 } from '../../components/atoms/Heading'
import EmphasisText from '../../components/atoms/Text/EmphasisText'

export default {
    title: 'atoms/Heading',
    component: Heading,
    args: {
        children: "Let's Communo",
    },
} as ComponentMeta<typeof Heading>

const Template: ComponentStory<typeof Heading> = (args) => <Heading {...args} />

export const Default = Template.bind({})
Default.args = {}

const H1Template: ComponentStory<typeof H1> = (args) => <H1 {...args} />
export const H1Title = H1Template.bind({})

const H2Template: ComponentStory<typeof H2> = (args) => <H2 {...args} />
export const H2Title = H2Template.bind({})

const H3Template: ComponentStory<typeof H3> = (args) => <H3 {...args} />
export const H3Title = H3Template.bind({})

const ChakraHeadingTemplate: ComponentStory<typeof ChakraHeading> = (args) => (
    <ChakraHeading {...args} />
)
export const FancyTitle = ChakraHeadingTemplate.bind({})
FancyTitle.args = {
    children: (
        <EmphasisText color="blue.400">{'I <em>Love</em> you'}</EmphasisText>
    ),
}
