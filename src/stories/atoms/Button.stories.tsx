import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { FaRainbow } from 'react-icons/fa'
import { BiAnalyse } from 'react-icons/bi'
import Button, {
    InfoButton,
    PrimaryButton,
    RainbowButon,
    SuccessButton,
} from '../../components/atoms/Button'

export default {
    title: 'atoms/Button',
    component: Button,
    args: {
        link: '#',
        children: "Let's Communo",
        width: 'auto',
        leftIcon: <BiAnalyse />,
    },
} as ComponentMeta<typeof Button>

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />

export const Default = Template.bind({})
Default.args = {}

export const Ghost = Template.bind({})
Ghost.args = {
    variant: 'ghost',
    colorScheme: 'orange',
}
export const Outline = Template.bind({})
Outline.args = {
    variant: 'outline',
    colorScheme: 'red',
}

const PrimaryTemplate: ComponentStory<typeof PrimaryButton> = (args) => (
    <PrimaryButton {...args} />
)

export const Primary = PrimaryTemplate.bind({})
Default.args = {}

export const BorderLite = Template.bind({})
BorderLite.args = {
    borderRadius: 'lg',
}

const RaindownTemplate: ComponentStory<typeof RainbowButon> = (args) => (
    <RainbowButon {...args} />
)
export const Rainbow = RaindownTemplate.bind({})
Rainbow.args = {
    leftIcon: undefined,
    rightIcon: <FaRainbow />,
}

const SuccessTemplate: ComponentStory<typeof SuccessButton> = (args) => (
    <SuccessButton {...args} />
)

export const Success = SuccessTemplate.bind({})
Success.args = {}

const InfoTemplate: ComponentStory<typeof InfoButton> = (args) => (
    <InfoButton {...args} />
)

export const Info = InfoTemplate.bind({})
Info.args = {}
