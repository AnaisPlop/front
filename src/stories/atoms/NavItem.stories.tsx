import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { BiAnalyse } from 'react-icons/bi'
import { Box } from '@chakra-ui/react'
import NavItem from '../../components/atoms/Menu/NavItem'

export default {
    title: 'atoms/NavItem',
    component: NavItem,
    args: {
        children: 'Pooling',
        to: '#',
        icon: BiAnalyse,
    },
} as ComponentMeta<typeof NavItem>

const Template: ComponentStory<typeof NavItem> = (args) => (
    <Box maxW="400" bg="gray.100">
        <NavItem {...args} />
    </Box>
)

export const Default = Template.bind({})
Default.args = {}

export const WithSubItems = Template.bind({})
WithSubItems.args = {
    subItems: [
        {
            name: 'Subitem 1',
            to: '#',
            display: { base: 'none', md: 'flex' },
        },
        {
            name: 'Subitem 2',
            to: '#',
            display: { base: 'none', md: 'flex' },
        },
    ],
}
