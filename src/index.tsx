import { ChakraProvider, ColorModeScript, theme } from '@chakra-ui/react'
import * as React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,
    createHttpLink,
} from '@apollo/client'
import { I18nextProvider } from 'react-i18next'
import { CookiesProvider } from 'react-cookie'
import reportWebVitals from './reportWebVitals'
import * as serviceWorker from './serviceWorker'
import MaterialShow from './routes/pooling/show'
import Home from './routes/home'
import MaterialBookingIndex from './routes/pooling/booking'
import 'react-toastify/dist/ReactToastify.css'
import './style/index.scss'
import Layout from './components/layout/Layout'
import Profile from './routes/profile'
import RequireAuth from './auth/RequireAuth'
import { AuthProvider } from './auth/AuthProvider'
import Logout from './auth/Logout'
import ApolloRefreshTokenAuthMiddleware from './auth/ApolloRefreshTokenAuthMiddleware'
import i18next from './translations'
import Confirm from './routes/register/confirm'
import Signin from './routes/register'
import MaterialNew from './routes/pooling/new'
import MaterialEdit from './routes/pooling/edit'
import MaterialBookingShow from './routes/pooling/booking/show'
import Why from './routes/why'
import Search from './routes/pooling/search'
import Pooling from './routes/pooling'
import Donate from './routes/donate'
import Contribute from './routes/contribute'
import DesignSystem from './routes/designSystem'

const link = createHttpLink({
    uri: `${process.env.REACT_APP_API_URL}/graphql`,
})
const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: ApolloRefreshTokenAuthMiddleware.concat(link),
})
const rootElement = document.getElementById('root')
if (!rootElement) throw new Error('Failed to find the root element')
const root = createRoot(rootElement)

root.render(
    <React.StrictMode>
        <ColorModeScript />
        <CookiesProvider />
        <ChakraProvider theme={theme}>
            <AuthProvider>
                <ApolloProvider client={client}>
                    <I18nextProvider i18n={i18next}>
                        <BrowserRouter>
                            <Routes>
                                <Route path="/" element={<Layout />}>
                                    <Route index element={<Home />} />
                                    <Route path="why" element={<Why />} />
                                    <Route
                                        path="contribute"
                                        element={<Contribute />}
                                    />
                                    <Route path="donate" element={<Donate />} />
                                    <Route
                                        path="register"
                                        element={<Signin mode="register" />}
                                    />
                                    <Route
                                        path="register/confirm/:id"
                                        element={<Confirm />}
                                    />
                                    <Route
                                        path="login"
                                        element={<Signin mode="login" />}
                                    />
                                    <Route path="logout" element={<Logout />} />
                                    <Route
                                        path="pooling"
                                        element={<Pooling />}
                                    />
                                    <Route
                                        path="pooling/search"
                                        element={<Search />}
                                    />
                                    <Route
                                        path="pooling/:id"
                                        element={<MaterialShow />}
                                    />
                                    <Route
                                        path="my/bookings"
                                        element={
                                            <RequireAuth>
                                                <MaterialBookingIndex />
                                            </RequireAuth>
                                        }
                                    />
                                    <Route
                                        path="my/bookings/:id"
                                        element={
                                            <RequireAuth>
                                                <MaterialBookingShow />
                                            </RequireAuth>
                                        }
                                    />
                                    <Route
                                        path="my/materials/:id/edit"
                                        element={
                                            <RequireAuth>
                                                <MaterialEdit />
                                            </RequireAuth>
                                        }
                                    />
                                    <Route
                                        path="my/materials/:id/edit/:step"
                                        element={
                                            <RequireAuth>
                                                <MaterialEdit />
                                            </RequireAuth>
                                        }
                                    />
                                    <Route
                                        path="pooling/new"
                                        element={
                                            <RequireAuth>
                                                <MaterialNew />
                                            </RequireAuth>
                                        }
                                    />
                                    <Route
                                        path="profile"
                                        element={
                                            <RequireAuth>
                                                <Profile />
                                            </RequireAuth>
                                        }
                                    />
                                    <Route
                                        path="ds"
                                        element={<DesignSystem />}
                                    />
                                    <Route
                                        path="*"
                                        element={
                                            <main style={{ padding: '1rem' }}>
                                                <p>Nothing here!</p>
                                            </main>
                                        }
                                    />
                                </Route>
                            </Routes>
                        </BrowserRouter>
                    </I18nextProvider>
                </ApolloProvider>
            </AuthProvider>
        </ChakraProvider>
    </React.StrictMode>
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorker.unregister()

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
