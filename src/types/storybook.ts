import { StoryAnnotations, AnnotatedStoryFn } from '@storybook/csf'
import { ReactFramework } from '@storybook/react'
import { ComponentProps, JSXElementConstructor } from 'react'

export type StoryObj<
    T extends keyof JSX.IntrinsicElements | JSXElementConstructor<unknown>
> = StoryAnnotations<ReactFramework, ComponentProps<T>>
export type StoryFn<
    T extends keyof JSX.IntrinsicElements | JSXElementConstructor<unknown>
> = AnnotatedStoryFn<ReactFramework, ComponentProps<T>>
